# -*- coding: utf-8 -*-
"""Example to test annotated module attributes.

Notes
-----
- This example file uses modern type annotations introduced in Python versions 3.9 and 3.10. \
The use of ``from __future__ import annotations`` is to allow such modern annotations to be used \
on Python versions as old as 3.7.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from collections.abc import Generator
    from typing import Any

from collections import OrderedDict

# NOTE: mod_attr_1 to mod_attr_4
# The extracted type should be the types defined in the type comments
# if "parse_type_comments" is true. Otherwise, it should be the type detected from their values.
mod_attr_1 = "string"  # type: int | str
mod_attr_2 = {}  # type: dict | OrderedDict
mod_attr_3 = [
    "item1",
    "item2",
    "item3",
    "item4",
    "item5",
    "item6",
    "item7",
    "item8",
]  # type: list | None
mod_attr_4 = False  # type: bool | None

# NOTE: mod_attr_5 to mod_attr_7
# All types should be the one defined in the *default_type* setting.
# If *allow_module_attributes_none_types* is *true*, mod_attr_5 type should be *None*.
mod_attr_5 = None
mod_attr_6 = OrderedDict()
mod_attr_7 = set()

annotated_mod_attr_1: str | None = None
annotated_mod_attr_2: list[
    tuple[str, str, str, str, str, str, str, str, str, str, str, str, str]
] = []


# NOTE: The docstring should be inserted after the type comment and before the rest of the comments.
# Successive runs should update/detect the right docstring.
def function_1(arg):
    # type: (bool) -> None
    # Comment
    # Comment
    pass


# NOTE: The docstring should be inserted before all comments.
# Successive runs should update/detect the right docstring.
def function_2(arg):
    # Comment
    # Comment
    pass


def demo(
    complacent=True,  # type: bool
    comma="yes",  # type: str
    placement=None,  # type: Any
    **kwargs,  # type: Any
):
    """Example for testing `issue 49 <https://github.com/KristoforMaynard/SublimeAutoDocstring/issues/49>`__.

    The type of these parameters should be obtained if the *parse_type_comments* setting
    is set to ``true``. The ``kwargs`` parameter type should be inserted only if
    the *allow_function_starred_parameters_types* setting is set to ``true``.
    """
    print("AutoDocstringFork saved my day!")


def echo_round() -> Generator[int, float, str]:
    """Example of a function that yields and returns."""
    sent = yield 0

    while sent >= 0:
        sent = yield round(sent)

    return "Done"


def infinite_stream(start: int) -> Generator[int, None, None]:
    """Example of a function that only yields."""
    while True:
        yield start
        start += 1


class ClassName:
    """Example for testing `issue 50 <https://github.com/KristoforMaynard/SublimeAutoDocstring/issues/50>`__
    and testing use_predefined_summaries preference.
    """

    attribute_1 = None  # type: bool
    annotated_attribute_1: str = None
    annotated_attribute_3: list[
        tuple[str, str, str, str, str, str, str, str, str, str, str, str, str]
    ] | None = None

    def __init__(self):
        self.attribute_2 = None
        self.annotated_attribute_2: str | None = None

    def do_something(
        self,
        arg1,
        arg2,
        kwarg1=[
            "item1",
            "item2",
            "item3",
            "item4",
            "item5",
            "item6",
            "item7",
            "item8",
        ],
        kwarg2=None,
    ) -> str:
        """Example of a function whose parameters have no annotations and whose types should
        be extracted from the defined parameters.
        """
        self.do_something_attr: str = kwarg1
        return "return_value"

    async def do_something_else(
        self,
        arg1: str,
        arg2,
        kwarg1: list[
            tuple[str, str, str, str, str, str, str, str, str, str, str, str, str, str, str]
        ]
        | None = None,
        *,
        kwarg2=None,
    ) -> str:
        """Example of an async function that has an annotation that spans across multiple lines."""
        self.do_something_else_attr = kwarg2

        if not arg1:
            raise RuntimeError

        if arg2 is None:
            raise SystemExit("SystemExit")

        return "return_value"

    def keep_doing_things(self, are_you_sure):
        # type: (bool) -> str
        return "Still working on it..."

    def __del__(self):
        # type: () -> None
        pass

    def __repr__(self) -> str:
        pass

    def __str__(self) -> str:
        pass

    def __bytes__(self) -> bytes:
        pass

    def __format__(self):
        pass

    def __lt__(self, other) -> bool:
        pass

    def __le__(self, other) -> bool:
        pass

    def __eq__(self, other) -> bool:
        pass

    def __ne__(self, other) -> bool:
        pass

    def __gt__(self, other) -> bool:
        pass

    def __ge__(self, other) -> bool:
        pass

    def __hash__(self):
        pass

    def __bool__(self):
        pass

    def __getattr__(self, name):
        pass

    def __setattr__(self, name, value):
        pass

    def __delattr__(self):
        pass

    def __dir__(self):
        pass

    def __get__(self):
        pass

    def __set__(self):
        pass

    def __delete__(self):
        pass
