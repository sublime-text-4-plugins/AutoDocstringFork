# -*- coding: utf-8 -*-
"""Business end of the AutoDocstring plugin.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from python_utils.sublime_text_utils.custom_classes import CustomListInputItem

import os

import sublime
import sublime_plugin

from . import Storage
from . import SyntaxManager
from . import logger
from . import settings
from . import utils
from .autodoc import autodoc
from python_utils.sublime_text_utils.utils import is_editable_view


__all__ = [
    "AutoDocstringForkCommand",
    "AutoDocstringForkAllCommand",
    "AutoDocstringForkConvertCommand",
    "AutoDocstringForkSnipCommand",
]


def is_python_file(view):
    """Check if view is a Python file.

    Checks file extension and syntax highlighting.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.

    Returns
    -------
    bool
        If view is a Python file.
    """
    filename = view.file_name()
    if filename:
        _, ext = os.path.splitext(filename)
    else:
        ext = ""
    if ext in [".py", ".pyx", ".pxd"]:
        return True

    syntax = view.settings().get("syntax").lower()
    if "python" in syntax or "cython" in syntax:
        return True

    return False


class BaseCommand(sublime_plugin.TextCommand):
    """Base command."""

    def is_enabled(self) -> bool:
        """Returns ``True`` if the command is able to be run at this time.
        The default implementation simply always returns ``True``.

        Returns
        -------
        bool
            If the command should be allowed to run.
        """
        commands_scope = settings.get("commands_scope")

        if commands_scope:
            view = self.view
            return all(
                (
                    is_editable_view(view),
                    view.score_selector(0, commands_scope) > 0,
                )
            )

        return True

    def is_visible(self) -> bool:
        """Returns ``True`` if the command should be shown in the menu at this time.
        The default implementation always returns ``True``.

        Returns
        -------
        bool
            If the command should be visible.
        """
        return self.is_enabled()


class AutoDocstringForkCommand(BaseCommand):
    """Auto docstring command."""

    def run(
        self, edit: sublime.Edit, default_qstyle: str | None = None, to_style: str | None = None
    ) -> None:
        """Insert/Revise docstring for the scope of the cursor location.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        default_qstyle : str | None, optional
            Description
        to_style : str | None, optional
            Description

        Returns
        -------
        None
            Halt execution.

        Raises
        ------
        TypeError
            Description
        """
        try:
            view = self.view

            if not settings.get("commands_scope"):
                if not is_python_file(view):
                    logger.error("Not a python file")
                    raise TypeError()

            SyntaxManager.set_syntax(view)

            # NOTE: Commented out unnecessary variable creation that is not used.
            # file_type = "cython" if "cython" in view.extract_scope(0) else "python"
            desired_style = utils.get_desired_style(view, desire=to_style)

            for region in view.sel():
                # NOTE: This was outside the loop in the original plugin.
                # I moved here so I can insert docstrings on multiple cursors.
                defs = utils.find_all_declarations(view, True)
                logger.debug("DEFINITIONS:: {}".format(defs))
                autodoc(
                    view,
                    edit,
                    region,
                    defs,
                    desired_style,
                    # file_type="python",
                    default_qstyle=default_qstyle,
                )
        except Exception:
            sublime.status_message("AutoDocstringFork is confused :-S, check console")
            raise
        else:
            sublime.status_message("AutoDoc'ed :-)")
        finally:
            sublime.set_timeout_async(lambda: SyntaxManager.reset_syntax(view), 100)

        return None


class AutoDocstringForkAllCommand(BaseCommand):
    """Auto docstring all command."""

    def run(
        self,
        edit: sublime.Edit,
        default_qstyle: str | None = None,
        to_style: str | None = None,
        update_only: bool = False,
    ) -> None:
        """Insert/Revise docstrings whole module.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        default_qstyle : str | None, optional
            Description
        to_style : str | None, optional
            Description
        update_only : bool, optional
            Description

        Returns
        -------
        None
            Halt execution.

        Raises
        ------
        TypeError
            Description

        No Longer Raises
        ----------------
        None
            Description
        """
        try:
            view = self.view

            if not settings.get("commands_scope"):
                if not is_python_file(view):
                    logger.error("Not a python file")
                    raise TypeError()

            SyntaxManager.set_syntax(view)

            # NOTE: Commented out unnecessary variable creation that is not used.
            # file_type = "cython" if "cython" in view.extract_scope(0) else "python"
            desired_style = utils.get_desired_style(view, desire=to_style)

            start_defs = utils.find_all_declarations(view, True)
            for i in range(len(start_defs)):
                defs = utils.find_all_declarations(view, True)
                d = defs[i]
                region = sublime.Region(d.b, d.b)
                autodoc(
                    view,
                    edit,
                    region,
                    defs,
                    desired_style,
                    # file_type="python",
                    default_qstyle=default_qstyle,
                    update_only=update_only,
                )
        except Exception:
            sublime.status_message("AutoDocstringFork is confused :-S, check console")
            raise
        else:
            sublime.status_message("AutoDoc'ed :-)")
        finally:
            sublime.set_timeout_async(lambda: SyntaxManager.reset_syntax(view), 100)

        return None


class AutoDocstringForkConvertCommand(BaseCommand):
    """Summary"""

    def run(self, edit: sublime.Edit, to_style: str | None = None, all: bool = False) -> None:
        """Called when the command is run.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        to_style : str | None, optional
            Description
        all : bool, optional
            Description

        Returns
        -------
        None
            Description
        """
        if not Storage.all_docstring_styles_quick_items:
            return

        to_style = to_style.lower() if to_style else to_style

        if to_style and to_style in Storage.all_docstring_styles:
            if all:
                self.view.run_command(
                    "auto_docstring_fork_all",
                    {"to_style": to_style, "update_only": True},
                )
            else:
                self.view.run_command("auto_docstring_fork", {"to_style": to_style})

        return None

    def input_description(self) -> str:
        """Allows a custom name to be show to the left of the cursor in the input box, instead of
        the default one generated from the command name.

        Returns
        -------
        str
            Input description.
        """
        return "Convert docstrings"

    def input(self, args: dict) -> sublime_plugin.ListInputHandler | None:
        """If this returns something other than None, the user will be prompted for an input before
        the command is run in the Command Palette.

        Parameters
        ----------
        args : dict
            Description

        Returns
        -------
        sublime_plugin.ListInputHandler | None
            Description
        """
        if args.get("to_style") is None:
            return ToStyleListInputHandler()

        return None


class ToStyleListInputHandler(sublime_plugin.ListInputHandler):
    """Summary"""

    def name(self) -> str:
        """The command argument name this input handler is editing.

        Returns
        -------
        str
            The command argument name.
        """
        return "to_style"

    def placeholder(self) -> str:
        """Placeholder text is shown in the text entry box before the user has entered anything.
        Empty by default.

        Returns
        -------
        str
            Description
        """
        return "Select docstring style"

    def list_items(self) -> list[CustomListInputItem | str]:
        """This method should return the items to show in the list.

        Returns
        -------
        list[CustomListInputItem | str]
            Description
        """
        if Storage.all_docstring_styles_quick_items:
            return Storage.all_docstring_styles_quick_items

        return ["Something went wrong!"]


class AutoDocstringForkSnipCommand(BaseCommand):
    """Auto docstring snip command."""

    def run(self, edit: sublime.Edit) -> None:
        """Insert/Revise docstring for the scope of the cursor location.

        Parameters
        ----------
        edit : sublime.Edit
            A Sublime Text ``Edit`` object.
        """
        view = self.view
        pt = view.sel()[0].a
        regA = sublime.Region(pt - 3, pt)
        qstyleA = view.substr(regA)
        assert qstyleA in ['"""', "'''"]
        view.replace(edit, regA, "")

        regB = sublime.Region(pt - 6, pt - 3)
        qstyleB = view.substr(regB)
        if qstyleB in ['"""', "'''"]:
            view.replace(edit, regB, "")

        args = {}
        args["default_qstyle"] = qstyleA
        view.window().run_command("auto_docstring_fork", args)
