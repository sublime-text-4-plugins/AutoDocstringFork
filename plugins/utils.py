# -*- coding: utf-8 -*-
"""
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from .docstring_styles.numpy import NumpyDocstring
    from .docstring_styles.google import GoogleDocstring

import ast
import re

from string import whitespace
from textwrap import dedent

import sublime

from . import all_decl_re
from . import docstring_styles
from . import logger
from . import settings


def find_all_declarations(view: sublime.View, include_module: bool = False) -> list[sublime.Region]:
    """Find all complete function/class declarations.

    Parameters
    ----------
    view : sublime.View
        The current Sublime Text ``View`` object.
    include_module : bool, optional
        Whether or not to include first character of file for the module docstring.

    Returns
    -------
    list[sublime.Region]
        The Sublime Text regions of all the declarations, from 'def'/'class' to the ':' inclusive.
    """
    defs: list[sublime.Region] = view.find_all(all_decl_re)
    # now prune out definitions found in comments / strings
    _defs: list[sublime.Region] = []

    if include_module:
        _defs.append(sublime.Region(0, 0))

    for d in defs:
        scope_name = view.scope_name(d.a)
        if not ("comment" in scope_name or "string" in scope_name):
            _defs.append(d)
    return _defs


def find_preceding_declaration(
    view: sublime.View, defs: list[sublime.Region], region: sublime.Region
) -> sublime.Region | None:
    """Find declaration immediately preceding the cursor.

    Parameters
    ----------
    view : sublime.View
        The current Sublime Text ``View`` object in which to search.
    defs : list[sublime.Region]
        List of all valid declarations (as regions).
    region : sublime.Region
        Region of the current selection.

    Returns
    -------
    sublime.Region | None
        Region of preceding declaration or ``None``.
    """
    preceding_defs: list[sublime.Region] = [d for d in defs if d.a <= region.a]
    logger.debug("PRECEDING_DEFINITIONS {}".format(preceding_defs))
    target: sublime.Region | None = None

    # for bypassing closures... as in, find the function that the
    # selection actually belongs to, don't just pick the first
    # preceding "def" since it could be a closure
    for d in reversed(preceding_defs):
        is_closure: bool = False
        block: str = view.substr(sublime.Region(view.line(d).a, view.line(region).b))
        block = dedent(block)
        block_start: int = 0

        if len(block) == 0:
            # happens on module level docstrings in modules that start
            # with a single blank line
            is_closure = False
        elif d.a == d.b == 0:
            # in case d is region(0, 0), aka module level
            is_closure = False
        elif block[0] in whitespace:
            logger.debug("Block 0 is whitespace")
            is_closure = True
        else:
            # ignore the whole declaration when determining closure-ness
            # NOTE: Added the try/except block as an attempt to fix issue 49.
            try:
                block_start = len(re.search(all_decl_re, block).group(0))
            except Exception:
                block_start = 0

            for line in block[block_start:].splitlines()[1:]:
                if len(line) > 0 and line[0] not in whitespace:
                    logger.debug("line[0] not whitespace: " + line)
                    is_closure = True
                    break

        if not is_closure:
            target = d
            break

    return target


def get_indentation(
    view: sublime.View, target: sublime.Region, module_decl: bool = False
) -> tuple[str, str, bool]:
    """Get indentation of a declaration and its body.

    Parameters
    ----------
    view : sublime.View
        The current Sublime Text ``View`` object.
    target : sublime.Region
        Region of the declaration of interest.
    module_decl : bool, optional
        Whether or not this is for doc'ing a module. It changes default body_indent_txt.

    Returns
    -------
    tuple[str, str, bool]
        def_indent_txt : str
            Indent of declaration.
        body_indent_txt : str
            Indent of body.
        has_indented_body : bool
            ``True`` if there is already text at body's indentation level.
    """
    # NOTE: view.indentation_level() is not a documented method.
    def_level: int = view.indentation_level(target.a)
    def_indent_txt: str = view.substr(view.find(r"\s*", view.line(target.a).a))

    # get indentation of the first non-whitespace char after the declaration
    nextline: sublime.Region = view.line(target.b).b
    next_char_reg: sublime.Region = view.find(r"\S", nextline)
    body: str = view.substr(view.line(next_char_reg))
    body_level: int = view.indentation_level(next_char_reg.a)
    body_indent_txt: str = body[: len(body) - len(body.lstrip())]
    has_indented_body: bool = True
    single_indent: str = ""

    # if no body text yet, attempt to auto-discover indentation
    if body_level > def_level:
        has_indented_body = True
    else:
        has_indented_body = False
        try:
            single_indent = def_indent_txt[: len(def_indent_txt) // def_level]
        except ZeroDivisionError:
            if module_decl:
                single_indent = ""
            else:
                single_indent = "    "
        body_indent_txt = def_indent_txt + single_indent

    return def_indent_txt, body_indent_txt, has_indented_body


def get_docstring(
    view: sublime.View,
    edit: sublime.Edit,
    target: sublime.Region,
    default_qstyle: str | None = None,
    extra_class_newlines: bool = True,
) -> tuple[sublime.Region | None, sublime.Region | None, str | None, bool, bool]:
    """Find a declaration's docstring.

    This will return a docstring even if it has to write one into the buffer. The idea is that
    all the annoying indentation discovery will be consolidated here, so in the future, all we
    have to do is run a replace on an existing docstring.

    Parameters
    ----------
    view : sublime.View
        The current Sublime Text ``View`` object.
    edit : sublime.Edit
        Sublime Text ``Edit`` object for inserting a new docstring if one does not already exist.
        ``None`` means "don't edit the buffer".
    target : sublime.Region
        Region of the declaration of interest.
    default_qstyle : str | None, optional
        Default qstyle.
    extra_class_newlines : bool, optional
        Class docstrings get an extra newline above and below, b/c PEP257. I find this unnecessary,
        but it's in a PEP, so it should at least be an option.

    Returns
    -------
    tuple[sublime.Region | None, sublime.Region | None, str | None, bool, bool]
        whole_region : sublime.Region
            Region of entire docstring (including quotes).
        docstr_region : sublime.Region
            Region of docstring excluding quotes.
        qstyle : str
            The character marking the ends of the docstring, will be one of ``[\"\"\", ''', ", ']``.
        new : bool
            ``True`` if we inserted a new docstring.
        module_level
            ``True`` if docstring is at module level.

    Note
    ----
    If no docstring exists, this will edit the buffer to add one if a :any:`sublime.Edit` object is given.
    """
    target_end_lineno, _ = view.rowcol(target.b)
    module_level: bool = target.a == target.b == 0

    # exclude the shebang line / coding line
    # by saying they're the declaration
    if module_level:
        cnt: int = -1
        while True:
            line: str = view.substr(view.line(cnt + 1))
            if (
                line.startswith("#!") or line.startswith("# -*-") or line.startswith("# pylint:")
            ):  # pylint: disable=bad-continuation
                cnt += 1
            else:
                break
        if cnt >= 0:
            target = sublime.Region(view.line(0).a, view.line(cnt).b)

    search_start: int = target.b

    next_chars_reg: sublime.Region = view.find(r"\S{1,4}", search_start)
    next_chars: str = view.substr(next_chars_reg)
    # NOTE: Fun fact. One can use booleans to perform arithmetic operations.
    # Basically, True is 1 and False is 0. Who would have thought.
    comment_hack_offset: bool = (
        view.substr(view.line(search_start + 1)).lstrip().startswith("# type:")
    )

    # hack for if there is a comment at the end of the declaration
    if (
        view.rowcol(next_chars_reg.a)[0] == target_end_lineno + comment_hack_offset
        and not module_level
        and next_chars
        and next_chars[0] == "#"
    ):
        search_start = view.line(search_start + comment_hack_offset).b
        next_chars_reg = view.find(r"\S{1,4}", search_start)
        next_chars = view.substr(next_chars_reg)

    same_line: bool = False

    if next_chars and view.rowcol(next_chars_reg.a)[0] == target_end_lineno + comment_hack_offset:
        same_line = True

    qstyle: str | None = None
    whole_region: sublime.Region | None = None
    docstr_region: sublime.Region | None = None
    literal_prefix: str = ""
    new: bool = False

    # for raw / unicode literals
    if next_chars.startswith(("r", "u")):
        literal_prefix = next_chars[0]
        next_chars = next_chars[1:]

    if next_chars.startswith(('"""', "'''")):
        qstyle = next_chars[:3]
    elif next_chars.startswith(('"', "'")):
        qstyle = next_chars[0]

    if qstyle:
        # there exists a docstring, get its region
        next_chars_reg.b = next_chars_reg.a + len(literal_prefix) + len(qstyle)
        docstr_end: sublime.Region = view.find(r"(?<!\\){0}".format(qstyle), next_chars_reg.b)
        if docstr_end.a < next_chars_reg.a:
            logger.info(f"Existing docstring on line {target_end_lineno} has no end?")
            return None, None, None, None, module_level

        whole_region = sublime.Region(next_chars_reg.a, docstr_end.b)
        docstr_region = sublime.Region(next_chars_reg.b, docstr_end.a)
        new = False

        # trim whitespace after docstring... having whitespace here seems
        # to mess with indentation for some reason
        if edit:
            after_quote_reg = sublime.Region(whole_region.b, view.line(whole_region.b).b)
            if len(view.substr(whole_region.b).strip()) == 0:
                view.replace(edit, after_quote_reg, "")
    elif edit is None:
        # no docstring exists, and don't make one
        return None, None, None, False, module_level
    else:
        # no docstring exists, but make / insert one
        qstyle = default_qstyle
        a: int = 0
        b: int = 0
        prefix: str = ""
        suffix: str = ""
        body_indent_txt: str = ""
        has_indented_body: bool = False

        _, body_indent_txt, has_indented_body = get_indentation(view, target, module_level)

        if module_level:
            # FIXME: whitespace is strange when inserting into a module that
            #        starts with 1-2 blank lines
            # NOTE: Changed from target.b to search_start.
            a, b = search_start, search_start
            prefix, suffix = "", ""
            if view.rowcol(a)[1] != 0:
                prefix = "\n"
            if same_line:
                suffix = "\n{0}".format(body_indent_txt)

            body_indent_txt = ""  # remove  whitespace added before module docstring
        elif same_line:
            # used if the function body starts on the same line as declaration
            # NOTE: Changed from target.b to search_start.
            a = search_start
            b = next_chars_reg.a
            prefix, suffix = "\n", "\n{0}".format(body_indent_txt)
            # hack for modules that start with comments
            if module_level:
                prefix = ""
        elif has_indented_body:
            # used if there is a function body at the next indent level
            # NOTE: Changed from target.b to search_start.
            a = view.full_line(search_start).b
            b = view.find(r"\s*", a).b
            prefix, suffix = "", "\n{0}".format(body_indent_txt)
        else:
            # used if there is no pre-existing indented text
            # NOTE: Changed from target.b to search_start.
            a = view.full_line(search_start).b
            b = a
            prefix, suffix = "", "\n"
            # hack if we're at the end of a file w/o a final \n
            # if not view.substr(view.full_line(target.b)).endswith("\n"):
            if not view.substr(view.full_line(search_start)).endswith("\n"):
                prefix = "\n"

        if extra_class_newlines:
            if view.substr(target).lstrip().startswith("class"):
                prefix += "\n"
                suffix *= 2

        stub: str = "{0}{1}{2}<FRESHLY_INSERTED>{2}{3}" "".format(
            prefix, body_indent_txt, qstyle, suffix
        )
        view.replace(edit, sublime.Region(a, b), stub)

        # NOTE: Changed from target.b to search_start.
        whole_region = view.find(
            "{0}<FRESHLY_INSERTED>{0}".format(qstyle), search_start, sublime.LITERAL
        )
        docstr_region = sublime.Region(whole_region.a + len(qstyle), whole_region.b - len(qstyle))
        new = True

    return whole_region, docstr_region, qstyle, new, module_level


def get_whole_block(view: sublime.View, target: sublime.Region) -> sublime.Region:
    """Find a region of all the lines that make up a class/function.

    Parameters
    ----------
    view : sublime.View
        The current Sublime Text ``View`` object.
    target : sublime.Region
        Region of the declaration of interest.

    Returns
    -------
    sublime.Region
        All lines in the class/function.

    Raises
    ------
    RuntimeError
        Unclosed string literal detected in file.
    """
    first_line: str = view.substr(view.line(target.a))
    leading_wspace: str = first_line[: len(first_line) - len(first_line.lstrip())]
    declaration_block: str = view.substr(sublime.Region(target.a, target.b))
    declaration_line_span: int = 1

    # NOTE: Take into account expanded functions/classes declarations.
    try:
        declaration_line_span = (re.search(all_decl_re, declaration_block).group(0)).count("\n") + 1
    except Exception:
        pass

    eoblock_row: int | None = None
    first_row: int = view.rowcol(target.a)[0]
    eof_row: int = view.rowcol(view.size())[0]

    for i in range(first_row + declaration_line_span, eof_row + 1):
        line_tp0: int = view.text_point(i, 0)
        line: str = view.substr(view.line(line_tp0)).rstrip()
        tp0_scope: str = view.scope_name(line_tp0)

        if not line or "comment" in tp0_scope or "string" in tp0_scope:
            continue

        if not (line.startswith(leading_wspace) and line[len(leading_wspace)] in " \t"):
            eoblock_row = i - 1
            break

    if eoblock_row is None:
        if "string" in view.scope_name(view.text_point(eof_row, 0)):
            raise RuntimeError("Unclosed string literal detected in file.")
        else:
            eoblock_row = eof_row

    block_region: sublime.Region = sublime.Region(
        view.line(target.a).a, view.text_point(eoblock_row + 1, 0)
    )

    return block_region


def get_attr_type(
    value: str | None, default_type: str, existing_type: str, allow_none_type: bool = False
) -> str:
    """Try to figure out type of attribute from declaration.

    If ``existing_type`` != ``default_type``, then ``existing_type`` is returned regardless
    of what's in this declaration.

    Parameters
    ----------
    value : str | None
        The right hand side of the equal sign.
    default_type : str
        Default text for the type.
    existing_type : str
        If attribute was already set, what was the type? Should equal defualt_type if
        the attribute was not previously set.
    allow_none_type : bool, optional
        If ``True``, allow default ``None`` values types to be ``None``.

    Returns
    -------
    str
        String describing the type of the attribute.
    """
    snippet_default: str = snipify(default_type)
    if (
        value is None
        or value == existing_type
        or existing_type not in [default_type, snippet_default]
    ):
        return existing_type

    value = value.strip()
    ret: str = default_type

    try:
        ret = ast.literal_eval(value).__class__.__name__
        if not allow_none_type and None.__class__.__name__ == ret:
            ret = default_type
    except ValueError as err:
        logger.debug(f"-> get_attr_type() error: \nvalue: '{value}'\nerror: '{err}'")
    except SyntaxError as err:
        logger.debug(f"-> get_attr_type() error: \nvalue: '{value}'\nerror: '{err}'")

    if ret == "NotImplementedType":
        ret = ret[: -len("Type")]

    if allow_none_type and ret == "NoneType":
        ret = ret[: -len("Type")]

    return ret


def get_desired_style(
    view: sublime.View, default: str = "numpy", desire: str | None = None
) -> type[GoogleDocstring] | type[NumpyDocstring]:
    """Get desired style/auto-discover from view if requested.

    Parameters
    ----------
    view : sublime.View
        A Sublime Text ``View`` object.
    default : str, optional
        Default docstring style.
    desire : str | None, optional
        If desire is a valid style, then always return that one.

    Returns
    -------
    type[GoogleDocstring] | type[NumpyDocstring]
        Subclass of :any:`docstring_styles.Docstring`, for now only Google or Numpy.
    """
    desire = desire.lower() if desire is not None else desire

    if desire and desire in docstring_styles.STYLE_LOOKUP:
        return docstring_styles.STYLE_LOOKUP[desire]

    style: str = settings.get("style", "numpy").lower()

    # do we want to auto-discover from the buffer?
    # TODO: cache auto-discovery using buffer_id?
    if style.startswith("auto"):
        try:
            default = style.split("_")[1]
        except IndexError:
            # default already set to numpy by kwarg
            pass

        defs: list[sublime.Region] = find_all_declarations(view, True)
        for d in defs:
            docstr_region: sublime.Region = get_docstring(view, None, d)[1]
            docstr_type: type[GoogleDocstring] | type[NumpyDocstring] | None = None

            if docstr_region is not None:
                # logger.debug("?? {}".format(docstr_region))
                docstr = view.substr(docstr_region)
                docstr_type = docstring_styles.detect_style(docstr)

            if docstr_type is not None:
                logger.debug("Docstring style auto-detected : '{}'".format(docstr_type))
                return docstr_type

        return docstring_styles.STYLE_LOOKUP[default]
    else:
        return docstring_styles.STYLE_LOOKUP[style]


def snipify(text: str, use_snippet: bool = True) -> str:
    """Conditionally convert text into a Sublime Text snippet.

    Parameters
    ----------
    text : str
        Text to convert to snippet.
    use_snippet : bool, optional
        If the text should be converted to snippet.

    Returns
    -------
    str
        *Snipified* or original text.
    """
    if use_snippet and text:
        return f"${{NUMBER:{text}}}"
        # text = r"${{NUMBER:{0}}}".format(text)
    return text
