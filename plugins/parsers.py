# -*- coding: utf-8 -*-
"""Python code parsers using :py:mod:`ast` module and also :py:mod:`parso`.

- :py:mod:`parso` is used to parse function parameters because they are accessible with just one \
function call and each parameter can easily be mapped to their default values. With :py:mod:`ast` \
it's ultra-mega-complicated to handle the parsed parameters and mapping them to their default \
values is practically impossible.
- On the other hand, parsing function exceptions with :py:mod:`ast` is ultra-mega-simple compared \
with how :py:mod:`parso` does it.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Any

import ast

from collections import OrderedDict
from textwrap import dedent

import sublime

from . import logger
from . import settings
from .docstring_styles import Parameter
from .parsers_utils import ClassDefVisitor
from .parsers_utils import FunctionDefVisitor
from .parsers_utils import ModuleVisitor
from .utils import get_attr_type
from .utils import get_whole_block
from .utils import snipify

_parse_error_message: str = """
Failure to parse string with ast.parse. Most likely, a regular expression \
failed to extract the body of a definition. Check again the regular expression output.
"""


def parse_function(
    view: sublime.View,
    target: sublime.Region,
    default_type: str,
    default_description: str,
    optional_tag: str = "optional",
) -> tuple[str, OrderedDict[str, Parameter], str | None, str, OrderedDict[str, Parameter]]:
    """Parse function parameters into an OrderedDict of :any:`Parameters`.

    Parameters
    ----------
    view : sublime.View
        The current Sublime Text ``View`` object.
    target : sublime.Region
        Region of the declaration of interest.
    default_type : str
        Default type text.
    default_description : str
        Default text.
    optional_tag : str, optional
        Tag included with type for kwargs when they are created.

    Returns
    -------
    tuple[str, OrderedDict[str, Parameter], str | None, str, OrderedDict[str, Parameter]]
        A tuple with the function name, its parameters, the function return value and the
        list of function exceptions as elements.

    Raises
    ------
    SyntaxError
        Failure to parse string with :py:meth:`ast.parse`. Most likely, a regular expression
        failed to extract the body of a definition. Check again the regular expression output.
    """
    default_description = snipify(default_description)
    function_region: sublime.Region = get_whole_block(view, target)
    function_string: str = dedent(view.substr(function_region))

    try:
        function_tree: ast.AST = ast.parse(function_string, mode="single", type_comments=True)
    except Exception as err:
        logger.error(_parse_error_message)
        logger.error(function_string)
        raise SyntaxError(err)

    visitor: FunctionDefVisitor = FunctionDefVisitor(
        function_string, parse_type_comments=settings.get("parse_type_comments", True)
    )
    visitor.visit(function_tree)
    function_data: Any = visitor.get_data()
    function_name: str = function_data.get("name", [""])[0]
    function_args: list = function_data.get("args", [])
    function_returns: str | None = function_data.get("returns", [None])[0]
    function_return_keyword: str | None = function_data.get("return_keyword", [None])[0]
    function_exceptions: list = function_data.get("exceptions", [])
    logger.debug(f"Function definition: {function_string}")
    logger.debug(f"AST tree dump: {ast.dump(function_tree)}")

    params_dict: OrderedDict[str, Parameter] = OrderedDict()
    excepts_dict: OrderedDict[str, Parameter] = OrderedDict()

    for i, (arg_name, arg_value, arg_anno, arg_is_annotated) in enumerate(function_args):
        paramtype: str | None = None

        if arg_name[0] == "*":
            paramtype = (
                arg_anno
                if settings.get("allow_function_starred_parameters_types", False)
                and arg_anno is not None
                else None
            )
        elif arg_anno is not None:
            paramtype = arg_anno
        else:
            paramtype = get_attr_type(
                arg_value,
                default_type,
                default_type,
                settings.get("allow_function_parameters_none_types", False),
            )

        if paramtype is not None:
            paramtype = snipify(paramtype)

        if optional_tag and arg_value is not None and paramtype:
            paramtype += ", {0}".format(optional_tag)

        p: Parameter = Parameter(
            [arg_name],
            paramtype,
            default_description,
            tag=i,
            annotated=bool(arg_anno),
        )
        params_dict[arg_name] = p

    for i, exce_name in enumerate(function_exceptions):
        if exce_name not in excepts_dict:
            e: Parameter = Parameter([exce_name], None, default_description, tag=i)
            excepts_dict[exce_name] = e

    visitor.destroy_data()
    del visitor

    return (
        function_name,
        params_dict,
        function_returns,
        function_return_keyword,
        excepts_dict,
    )


def parse_class(
    view: sublime.View, target: sublime.Region, default_type: str, default_description: str
) -> tuple[str, OrderedDict[str, Parameter]]:
    """Scan a class' code and look for attributes.

    Parameters
    ----------
    view : sublime.View
        The current Sublime Text ``View`` object.
    target : sublime.Region
        Region of the declaration of interest.
    default_type : str
        Default type text.
    default_description : str
        Default text.

    Returns
    -------
    tuple[str, OrderedDict[str, Parameter]]
        A tuple with the class name and the class attributes as elements.

    Raises
    ------
    SyntaxError
        Failure to parse string with :py:meth:`ast.parse`. Most likely, a regular expression
        failed to extract the body of a definition. Check again the regular expression output.
    """
    default_description = snipify(default_description)

    attribs: OrderedDict[str, Parameter] = OrderedDict()

    class_region: sublime.Region = get_whole_block(view, target)
    class_string: str = dedent(view.substr(class_region))

    try:
        class_tree: ast.AST = ast.parse(class_string, mode="single", type_comments=True)
    except Exception as err:
        logger.error(_parse_error_message)
        logger.error(class_string)
        raise SyntaxError(err)

    visitor: ClassDefVisitor = ClassDefVisitor(
        class_string, parse_type_comments=settings.get("parse_type_comments", True)
    )
    visitor.visit(class_tree)
    class_data: dict = visitor.get_data()
    class_name: str = class_data["name"]
    class_attributes: dict = class_data["attributes"]
    logger.debug(f"Class definition: {class_string}")
    logger.debug(f"AST tree dump: {ast.dump(class_tree)}")

    for attr_name, (attr_value, attr_anno) in class_attributes.items():
        if attr_name.startswith("_") and settings.get("ignore_private_class_attributes", True):
            continue

        # discover data type from declaration
        if attr_name in attribs:
            existing_type = attribs[attr_name].types
        else:
            existing_type = default_type

        paramtype: str = attr_anno

        if not paramtype:
            paramtype = get_attr_type(
                attr_value,
                default_type,
                existing_type,
                settings.get("allow_class_attributes_none_types", False),
            )

        if not paramtype.startswith(r"${NUMBER:"):
            paramtype = snipify(paramtype)

        tag: int = len(attribs)

        if attr_name in attribs:
            tag = attribs[attr_name].tag

        param = Parameter(
            [attr_name], paramtype, default_description, tag=tag, annotated=bool(attr_anno)
        )
        attribs[attr_name] = param

    visitor.destroy_data()
    del visitor

    return (class_name, attribs)


def parse_module_attributes(
    view: sublime.View, default_type: str, default_description: str
) -> OrderedDict[str, Parameter]:
    """Scan a module's code and look for attributes.

    Parameters
    ----------
    view : sublime.View
        The current Sublime Text ``View`` object.
    default_type : str
        Default type text.
    default_description : str
        Default text.

    Returns
    -------
    OrderedDict[str, Parameter]
        An OrderedDict containing Parameter instances.

    Raises
    ------
    SyntaxError
        Failure to parse string with :py:meth:`ast.parse`. Most likely, a regular expression
        failed to extract the body of a definition. Check again the regular expression output.
    """
    default_description = snipify(default_description)
    attribs: OrderedDict[str, Parameter] = OrderedDict()
    module_string = dedent(view.substr(sublime.Region(0, view.size())))

    try:
        module_tree: ast.AST = ast.parse(module_string, type_comments=True)
    except Exception as err:
        logger.error(_parse_error_message)
        logger.error(module_string)
        raise SyntaxError(err)

    visitor: ModuleVisitor = ModuleVisitor(
        module_string, parse_type_comments=settings.get("parse_type_comments", True)
    )
    visitor.visit(module_tree)
    module_data: dict = visitor.get_data()
    module_attributes: dict = module_data["attributes"]
    logger.debug(f"Module definition: {module_string}")
    logger.debug(f"AST tree dump: {ast.dump(module_tree)}")

    for attr_name, (attr_value, attr_anno) in module_attributes.items():
        if attr_name.startswith("_") and settings.get("ignore_private_module_attributes", True):
            continue

        existing_type: str = default_type

        if attr_name in attribs:
            existing_type = attribs[attr_name].types

        tag: int = len(attribs)

        if attr_name in attribs:
            tag = attribs[attr_name].tag

        paramtype: str = attr_anno

        if not paramtype:
            paramtype = get_attr_type(
                attr_value,
                default_type,
                existing_type,
                settings.get("allow_module_attributes_none_types", False),
            )

        if not paramtype.startswith(r"${NUMBER:"):
            paramtype = snipify(paramtype)

        attribs[attr_name] = Parameter(
            [attr_name], paramtype, default_description, tag=tag, annotated=bool(attr_anno)
        )

    visitor.destroy_data()
    del visitor

    return attribs
