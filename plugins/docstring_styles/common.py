# -*- coding: utf-8 -*-
"""Summary
"""
from __future__ import annotations

from textwrap import dedent

from .. import logger


def dedent_docstr(s, n=1):
    """Dedent all lines except first n lines

    Parameters
    ----------
    s : type
        Some text to dedent.
    n : int
        Number of lines to skip, (``n == 0`` is a normal dedent, ``n == 1`` is useful for whole docstrings).

    Returns
    -------
    str
        Dedented docstring.
    """
    lines = s.splitlines(keepends=True)
    if lines:
        first_n_lines = "".join([l.lstrip(" \t") for l in lines[:n]])
        dedented = dedent("".join(lines[n:]))
        return first_n_lines + dedented
    else:
        return ""


def dedent_verbose(s, n=1):
    """Summary

    Parameters
    ----------
    s : type
        Some text to dedent.
    n : int
        Number of lines to skip, (``n == 0`` is a normal dedent, ``n == 1`` is useful for whole docstrings).

    Returns
    -------
    tuple
        Section indent and dedented docstring.
    """
    new = dedent_docstr(s, n=n)

    s_split = s.splitlines(keepends=True)
    new_split = new.splitlines(keepends=True)
    i, ind = 0, -1
    for i in range(n, len(s_split)):
        if s_split[i].strip():
            ind = s_split[i].find(new_split[i])
            break
    if ind >= 0:
        indent = s_split[i][:ind]
    else:
        indent = ""

    return indent, new


def indent_docstr(s, indent, n=1, trim=True):
    """Add common indentation to all lines except first.

    Parameters
    ----------
    s : str
        Docstring starting at indentation level 0.
    indent : str
        Text used for indentation, in practice this will be the level of the declaration + 1.
    n : int
        Don't indent first n lines.
    trim : bool
        Trim whitespace (' \t') out of blank lines.

    Returns
    -------
    str
        ``s`` with common indentation applied.
    """
    lines = s.splitlines(keepends=True)
    for i in range(n, len(lines)):
        if lines[i].strip() or not trim:
            lines[i] = "{0}{1}".format(indent, lines[i])
        else:
            lines[i] = lines[i].strip(" \t")
    return "".join(lines)


def count_leading_newlines(s):
    """Count number of leading newlines.

    This includes newlines that are separated by other whitespace.

    Parameters
    ----------
    s : str
        Text to count leading new lines on.

    Returns
    -------
    int
        Number of leading lines.
    """
    return s[: -len(s.lstrip())].count("\n")


def count_trailing_newlines(s):
    """Count number of trailing newlines.

    This includes newlines that are separated by other whitespace.

    Parameters
    ----------
    s : str
        Text to count trailing new lines on.

    Returns
    -------
    int
        Number of trailing lines.
    """
    return s[len(s.rstrip()):].count("\n")


def with_bounding_newlines(s, nleading=0, ntrailing=0, nl="\n"):
    """Return ``s`` with at least ``n`` leading and ``n`` trailing newlines.

    This includes newlines that are separated by other whitespace.

    Parameters
    ----------
    s : str
        Description
    nleading : int, optional
        Number of leading new lines.
    ntrailing : int, optional
        Number of trailing new lines.
    nl : str, optional
        New line character.

    Returns
    -------
    str
        Modified ``s``.
    """
    return "{0}{1}{2}".format(
        nl * (nleading - count_leading_newlines(s)),
        s,
        nl * (ntrailing - count_trailing_newlines(s)),
    )


def strip_newlines(s, nleading=0, ntrailing=0):
    """Strip at most nleading and ntrailing newlines from ``s``.

    Parameters
    ----------
    s : str
        String to strip new lines from.
    nleading : int, optional
        Strip ``n`` leading new lines.
    ntrailing : int, optional
        Strip ``n`` trailing new lines.

    Returns
    -------
    str
        ``s`` with stripped new lines.
    """
    for _ in range(nleading):
        if s.lstrip(" \t")[0] == "\n":
            s = s.lstrip(" \t")[1:]
        elif s.lstrip(" \t")[0] == "\r\n":
            s = s.lstrip(" \t")[2:]

    for _ in range(ntrailing):
        if s.rstrip(" \t")[-2:] == "\r\n":
            s = s.rstrip(" \t")[:-2]
        elif s.rstrip(" \t")[-1:] == "\n":
            s = s.rstrip(" \t")[:-1]

    return s


class Parameter(object):
    """Summary

    Attributes
    ----------
    annotated : TYPE
        Description
    descr_only : TYPE
        Description
    description : TYPE
        Description
    meta : TYPE
        Description
    names : TYPE
        Description
    tag : TYPE
        Description
    types : TYPE
        Description
    """

    names = None
    types = None
    description = None
    tag = None
    descr_only = None
    meta = None

    def __init__(
        self,
        names,
        types,
        description,
        tag=None,
        descr_only=False,
        annotated=False,
        **kwargs,
    ):
        """See :py:meth:`__init__`.

        Parameters
        ----------
        names : list
            List of names.
        types : str
            String describing data types.
        description : str
            Description text.
        tag : int
            Some meaningful index? not fleshed out yet.
        descr_only : bool
            Only description is useful.
        annotated : bool, optional
            Description
        **kwargs
            Keyword arguments.
        """
        assert names is not None
        if description is None:
            description = ""
        self.names = names
        self.types = types
        self.description = description
        self.tag = tag
        self.descr_only = descr_only
        self.annotated = annotated
        self.meta = kwargs


class Section(object):
    """Summary

    Attributes
    ----------
    alias : TYPE
        Description
    ALIASES : dict
        Description
    args : TYPE
        Description
    args_formatter : TYPE
        Description
    args_parser : TYPE
        Description
    formatter_override : TYPE
        Description
    heading : TYPE
        Description
    indent : str
        Description
    is_formatted : bool
        Description
    meta : TYPE
        Description
    PARSERS : dict
        Description
    section_indent : str
        Description
    text : TYPE
        Description
    """

    ALIASES = {}
    PARSERS = {}

    is_formatted = None
    args = None
    args_parser = None
    args_formatter = None

    heading = None
    alias = None
    _text = None
    section_indent = ""
    indent = "    "
    meta = None

    formatter_override = None

    def __init__(self, heading, text="", indent=None, **kwargs):
        """See :py:meth:`__init__`.

        Parameters
        ----------
        heading : str
            Heading of the section (should be title case).
        text : str, optional
            Section text.
        indent : str, optional
            Used by some formatters.
        **kwargs
            Keyword arguments.
        """
        self.heading = heading
        self.alias = self.resolve_alias(heading)

        if self.alias in self.PARSERS:
            parser, formatter = self.PARSERS[self.alias]
            self.args_parser = parser
            self.args_formatter = formatter
            self.is_formatted = True
        else:
            self.is_formatted = False

        if indent is not None:
            self.indent = indent

        self.text = text
        self.meta = kwargs

        logger.debug(
            "Create section '{}' ({}) with args : '{}'".format(self.heading, self.alias, self.args)
        )

    @classmethod
    def from_section(cls, sec):
        """Summary

        Parameters
        ----------
        sec : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        new_sec = cls(sec.alias)
        new_sec._text = sec._text  # pylint: disable=protected-access
        # when changing styles, the indentation should change to better fit
        # the new style
        # new_sec.section_indent = sec.section_indent
        # new_sec.indent = sec.indent
        if hasattr(sec, "args"):
            new_sec.args = sec.args
        return new_sec

    @classmethod
    def resolve_alias(cls, heading):
        """Summary

        Parameters
        ----------
        heading : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        titled_heading = heading.title()
        try:
            return cls.ALIASES[titled_heading]
        except KeyError:
            return heading

    @property
    def text(self):
        """Summary

        Returns
        -------
        TYPE
            Description
        """
        if self.formatter_override is not None:
            s = self.formatter_override(self)  # pylint: disable=not-callable
        elif self.args_formatter is not None:
            s = self.args_formatter(self)
        else:
            s = self._text
        return s

    @text.setter
    def text(self, val):
        """Summary

        Parameters
        ----------
        val : TYPE
            Description
        """
        val = strip_newlines(val, ntrailing=1)
        if self.args_parser is not None:
            self.args = self.args_parser(self, val)
        else:
            section_indent, self._text = dedent_verbose(val, n=0)
            # don't overwrite section indent if val isn't indented
            if section_indent:
                self.section_indent = section_indent
