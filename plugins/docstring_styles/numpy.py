# -*- coding: utf-8 -*-
from __future__ import annotations

import re

from .. import logger

from .common import Parameter
from .common import dedent_verbose
from .common import indent_docstr
from .common import with_bounding_newlines
from .napoleon import NapoleonDocstring
from .napoleon import NapoleonSection


class NumpySection(NapoleonSection):
    """Summary

    Attributes
    ----------
    indent : str
        Description
    PARSERS : TYPE
        Description
    """

    indent = "    "

    @staticmethod
    def finalize_param(s, i):
        """Summary

        Parameters
        ----------
        s : TYPE
            Description
        i : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        meta = {}
        # NOTE: Original regex. I had to add the | character because otherwise it would remove
        # existent descriptions of return values with annotations containing |. I don't
        # know if it's allowed by Numpy convention. What I know is that it isn't possible to
        # change the | for commas and changing the annotation in any other way makes no sense.
        # Furthermore, building the documentation with Sphinx doesn't throw any errors and the
        # resulting documents (at least HTML) are perfectly readable.
        # _r = r"\s*([^,\s]+(?:\s*,\s*[^,\s]+)*)\s*(?::\s*(.*?))?[^\S\n]*?\n(\s+.*)"
        _r = r"\s*([^,\|\s]+(?:\s*[,|\|]\s*[^,\|\s]+)*)\s*(?::\s*(.*?))?[^\S\n]*?\n(\s+.*)"
        m = re.match(_r, s, re.DOTALL)
        if m:
            names, typ, desc = m.groups()
            # FIXME hack, name for numpy parameters is always a list of names
            # to support the multiple parameters per description option in
            # numpy docstrings
            names = [n.strip() for n in names.split(",")]
            meta["indent"], descr = dedent_verbose(desc, 0)
            descr_only = False
        else:
            names = ["{0}".format(i)]
            typ = ""
            descr = s
            descr_only = True
        return Parameter(names, typ, descr, tag=i, descr_only=descr_only, **meta)

    def param_parser(self, text):
        """Summary

        Parameters
        ----------
        text : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        logger.debug("[NumpySection] section '{}' starts parsing".format(self.alias))
        return self.param_parser_common(text)

    def param_formatter(self):
        """Summary

        Returns
        -------
        TYPE
            Description
        """
        # NOTE: there will be some tricky business if there is a
        # section break done by "resuming unindented text"
        logger.debug("[NumpySection] section '{}' starts formatting".format(self.alias))

        s = ""
        # already_seen = {}
        for param in self.args.values():
            if param.descr_only:
                s += with_bounding_newlines(param.description, ntrailing=1)
            else:
                p = "{0}".format(", ".join(param.names))
                if param.types:
                    types = param.types.strip()
                    if types:
                        p = "{0} : {1}".format(p, param.types.strip())
                p = with_bounding_newlines(p, ntrailing=1)
                if param.description:
                    p += indent_docstr(
                        param.description, param.meta.get("indent", self.indent), n=0
                    )
                s += with_bounding_newlines(p, ntrailing=1)
        return s

    PARSERS = {
        "Parameters": (param_parser, param_formatter),
        "Other Parameters": (param_parser, param_formatter),
        "Deleted Parameters": (param_parser, param_formatter),
        "Keyword Arguments": (param_parser, param_formatter),
        "Attributes": (param_parser, param_formatter),
        "Deleted Attributes": (param_parser, param_formatter),
        "Raises": (param_parser, param_formatter),
        "No Longer Raises": (param_parser, param_formatter),
        "Returns": (param_parser, param_formatter),
        "Yields": (param_parser, param_formatter),
        "No Longer Returned": (param_parser, param_formatter),
        "No Longer Yielded": (param_parser, param_formatter),
    }


class NumpyDocstring(NapoleonDocstring):
    """Summary

    Attributes
    ----------
    PREFERRED_PARAMS_ALIAS : str
        Description
    SECTION_RE : str
        Description
    SECTION_STYLE : TYPE
        Description
    STYLE_NAME : str
        Description
    """

    STYLE_NAME = "numpy"
    SECTION_STYLE = NumpySection
    SECTION_RE = r"^([A-Za-z0-9][A-Za-z0-9 \t]*)\s*\n-+\s*?$\r?\n?"
    PREFERRED_PARAMS_ALIAS = "Parameters"

    @classmethod
    def detect_style(cls, docstr):
        """Summary

        Parameters
        ----------
        docstr : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        m = re.search(cls.SECTION_RE, docstr, re.MULTILINE)
        return m is not None

    @staticmethod
    def _extract_section_name(sec_re_result):
        """Summary

        Parameters
        ----------
        sec_re_result : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        return sec_re_result.strip().rstrip("-").rstrip()

    @staticmethod
    def _format_section_text(heading, body):
        """Summary

        Parameters
        ----------
        heading : TYPE
            Description
        body : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        return "{0}\n{1}\n{2}".format(heading, "-" * len(heading), body)
