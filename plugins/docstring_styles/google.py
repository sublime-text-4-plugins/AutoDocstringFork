# -*- coding: utf-8 -*-
from __future__ import annotations

import re

from .. import logger

from .common import Parameter
from .common import dedent_verbose
from .common import indent_docstr
from .common import with_bounding_newlines
from .napoleon import NapoleonDocstring
from .napoleon import NapoleonSection


class GoogleSection(NapoleonSection):
    """Summary

    Attributes
    ----------
    indent : str
        Description
    PARSERS : TYPE
        Description
    section_indent : str
        Description
    """

    section_indent = "    "
    indent = "    "

    @staticmethod
    def finalize_param(s, tag):
        """Summary

        Parameters
        ----------
        s : type
            Description
        tag : int
            Index of param? not fleshed out yet.

        Returns
        -------
        TYPE
            Description
        """
        meta = {}
        # NOTE: Original regex.
        # _r = r"([^,\s]+(?:\s*,\s*[^,\s]+)*\s*)(?:\((.*)\))?\s*:\s*(.*)"
        _r = r"([^,\|\s]+(?:\s*[,|\|]\s*[^,\|\s]+)*\s*)(?:\((.*)\))?\s*:\s*(.*)"
        m = re.match(_r, s, re.DOTALL | re.MULTILINE)
        if m:
            names, typ, descr = m.groups()
            names = [n.strip() for n in names.split(",")]
            meta["indent"], descr = dedent_verbose(descr, n=1)
            descr_only = False
        else:
            names = ["{0}".format(tag)]
            typ = ""
            descr = s
            descr_only = True
        return Parameter(names, typ, descr, tag=tag, descr_only=descr_only, **meta)

    def param_parser(self, text):
        """Summary

        Parameters
        ----------
        text : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        logger.debug("[GoogleSection] section '{}' starts parsing".format(self.alias))
        return self.param_parser_common(text)

    def param_formatter(self):
        """Summary

        Returns
        -------
        TYPE
            Description
        """
        logger.debug("[GoogleSection] section '{}' starts formatting".format(self.alias))

        s = ""
        for param in self.args.values():
            if param.descr_only:
                s += with_bounding_newlines(param.description, ntrailing=1)
            else:
                if len(param.names) > 1:
                    logger.warning(
                        "section '{}' : Google docstrings don't allow > 1 "
                        "parameter per description".format(self.alias)
                    )
                p = "{0}".format(", ".join(param.names))
                if param.types:
                    types = param.types.strip()
                    if types:
                        p = "{0} ({1})".format(p, types)
                if param.description:
                    desc = indent_docstr(param.description, param.meta.get("indent", self.indent))
                    p = "{0}: {1}".format(p, desc)
                s += with_bounding_newlines(p, ntrailing=1)
        return s

    PARSERS = {
        "Parameters": (param_parser, param_formatter),
        "Other Parameters": (param_parser, param_formatter),
        "Deleted Parameters": (param_parser, param_formatter),
        "Keyword Arguments": (param_parser, param_formatter),
        "Attributes": (param_parser, param_formatter),
        "Deleted Attributes": (param_parser, param_formatter),
        "Raises": (param_parser, param_formatter),
        "No Longer Raises": (param_parser, param_formatter),
        "Returns": (param_parser, param_formatter),
        "Yields": (param_parser, param_formatter),
        "No Longer Returned": (param_parser, param_formatter),
        "No Longer Yielded": (param_parser, param_formatter),
    }


class GoogleDocstring(NapoleonDocstring):
    """Summary

    Attributes
    ----------
    PREFERRED_PARAMS_ALIAS : str
        Description
    SECTION_RE : str
        Description
    SECTION_STYLE : TYPE
        Description
    STYLE_NAME : str
        Description
    """

    STYLE_NAME = "google"
    SECTION_STYLE = GoogleSection
    SECTION_RE = r"^[A-Za-z0-9][A-Za-z0-9 \t]*:\s*$\r?\n?"
    PREFERRED_PARAMS_ALIAS = "Args"

    @classmethod
    def detect_style(cls, docstr):
        """Summary

        Parameters
        ----------
        docstr : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        m = re.search(cls.SECTION_RE, docstr, re.MULTILINE)
        return m is not None

    @staticmethod
    def _extract_section_name(sec_re_result):
        """Summary

        Parameters
        ----------
        sec_re_result : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        return sec_re_result.strip().rstrip(":").rstrip()

    @staticmethod
    def _format_section_text(heading, body):
        """Summary

        Parameters
        ----------
        heading : TYPE
            Description
        body : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        return "{0}:\n{1}".format(heading, body)
