# -*- coding: utf-8 -*-
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from .napoleon import NapoleonDocstring
    from typing import Any

from collections import OrderedDict

from .. import logger

from .common import Parameter
from .common import dedent_docstr
from .google import GoogleDocstring
from .napoleon import Docstring
from .numpy import NumpyDocstring

__all__ = [
    "detect_style",
    "STYLE_LOOKUP",
    "Parameter",
    "make_docstring_obj",
]


STYLE_LOOKUP: OrderedDict[str, type[GoogleDocstring] | type[NumpyDocstring]] = OrderedDict(
    [("numpy", NumpyDocstring), ("google", GoogleDocstring)]
)


def make_docstring_obj(
    docstr: str, default: str | type[NapoleonDocstring] = "numpy", template_order: bool = False
) -> NapoleonDocstring:
    """Detect docstring style and create a ``NapoleonDocstring`` object.

    Parameters
    ----------
    docstr : str
        source docstring
    default : str | type[NapoleonDocstring], optional
        ``google``, ``numpy`` or subclass of NapoleonDocstring.
    template_order : bool, optional
        If ``True``, reorder the sections to match the order they appear in the template.

    Returns
    -------
    NapoleonDocstring
        A subclass of ``NapoleonDocstring``.
    """
    typ = detect_style(docstr)
    logger.debug(
        f"[make_docstring_obj] from {repr(typ)} to {repr(default)}"
    )

    if typ is None:
        if isinstance(default, str):
            typ = STYLE_LOOKUP[default.lower()]
        elif issubclass(default, Docstring):
            typ = default

    return typ(docstr, template_order=template_order)


def detect_style(docstr):
    """Detect ``docstr`` style from existing docstring.

    Parameters
    ----------
    docstr : str
        Docstring whose style we want to know.

    Returns
    -------
    class
        One of [GoogleDocstring, NumpyDocstring, None]; ``None`` means no match.
    """
    docstr = dedent_docstr(docstr)

    for c in STYLE_LOOKUP.values():
        if c.detect_style(docstr):
            return c
    return None
