# -*- coding: utf-8 -*-
from __future__ import annotations

import re

from collections import OrderedDict
from itertools import islice

from .. import logger

from .common import Parameter
from .common import Section
from .common import count_trailing_newlines
from .common import dedent_docstr
from .common import indent_docstr
from .common import with_bounding_newlines


class Docstring(object):
    """Handle parsing/modifying/writing docstrings.

    Attributes
    ----------
    PREFERRED_PARAMS_ALIAS : str
        Description
    SECTION_STYLE : TYPE
        Description
    sections : TYPE
        Description
    STYLE_NAME : str
        Description
    TEMPLATE : TYPE
        Description
    trailing_newlines : TYPE
        Description
    """

    STYLE_NAME = "none"
    SECTION_STYLE = Section
    TEMPLATE = OrderedDict([("Summary", None)])
    PREFERRED_PARAMS_ALIAS = "Args"
    SECTIONS_THAT_NEED_INDENT_HACK = (
        "Other Parameters",
        "Parameters",
        "Returns",
        "Yields",
        "Attributes",
    )

    sections = None
    trailing_newlines = None

    def __init__(self, docstr, template_order=False):
        """See :py:meth:`__init__`.

        Parameters
        ----------
        docstr : Docstring, str
            Some existing docstring.
        template_order : bool, optional
            If ``True``, reorder the sections to match the order they appear in the template.
        """
        if isinstance(docstr, Docstring):
            self.sections = docstr.sections
            self.trailing_newlines = docstr.trailing_newlines
            if not isinstance(docstr, type(self)):

                # fixme, this is kinda hacky
                make_new_sec = self.SECTION_STYLE.from_section
                for sec_name, sec in list(docstr.sections.items()):
                    # when the section should not exists
                    # i.e. when a section was generated, but isn't needed anymore
                    # e.g. when there isn't any exception raised
                    if sec:
                        docstr.sections[sec_name] = make_new_sec(sec)
                    else:
                        # deleting section that shouldn't be here
                        # including those generated with template_order=True
                        del docstr.sections[sec_name]

                # ok, this way of changing indentation is a thunder hack
                # NOTE: I modified this to modify ALL SECTIONS because converting
                # from Google style to Numpy style produced unindented descriptions.
                # So, I grab the PARSERS attribute from the first section I can find and
                # iterate over ALL of the sections.
                if docstr.sections:
                    dummy_section = next(
                        (sec for sec in docstr.sections.values() if sec is not None), None
                    )

                    if dummy_section is not None:
                        parsers = dummy_section.PARSERS
                        for sec_name in parsers:
                            if (
                                sec_name in docstr.sections
                                and docstr.sections[sec_name] is not None
                            ):
                                if sec_name == "Parameters":
                                    self.get_section(sec_name).heading = self.PREFERRED_PARAMS_ALIAS

                                for arg in self.get_section(sec_name).args.values():
                                    arg.meta["indent"] = self.get_section(sec_name).indent
        elif isinstance(docstr, str):
            if template_order:
                self.sections = self.TEMPLATE.copy()
            else:
                self.sections = OrderedDict()
            self._parse(docstr)

    def _parse(self, s):
        """Parse docstring into meta data.

        Parameters
        ----------
        s : str
            Docstring.

        Raises
        ------
        NotImplementedError
            Description
        """
        raise NotImplementedError("_parse is an abstract method")

    def format(self, top_indent):
        """Format docstring into a string.

        Parameters
        ----------
        top_indent : str
            Indentation added to all but the first lines.

        Raises
        ------
        NotImplementedError
            Description
        """
        raise NotImplementedError("format is an abstract method")

    def update_parameters(self, params):
        """Summary

        Parameters
        ----------
        params : TYPE
            Description

        Raises
        ------
        NotImplementedError
            Description
        """
        raise NotImplementedError("update_parameters is an abstract method")

    def update_return_type(
        self, ret_name, ret_type, default_description="Description", keyword="return"
    ):
        """Summary

        Parameters
        ----------
        ret_name : TYPE
            Description
        ret_type : TYPE
            Description
        default_description : str, optional
            Description
        keyword : str, optional
            Description

        Raises
        ------
        NotImplementedError
            Description
        """
        raise NotImplementedError("update_return_type is an abstract method")

    def update_attributes(
        self, attribs: OrderedDict[str, Parameter], alpha_order: bool = True
    ) -> None:
        """Summary

        Parameters
        ----------
        attribs : OrderedDict
            Params objects keyed by their names.
        alpha_order : bool, optional
            Description

        Raises
        ------
        NotImplementedError
            Description
        """
        raise NotImplementedError("update_attributes is an abstract method")

    def update_exceptions(self, attribs, alpha_order=True):
        """Summary

        Parameters
        ----------
        attribs : OrderedDict
            Params objects keyed by their names.
        alpha_order : bool, optional
            Description

        Raises
        ------
        NotImplementedError
            Description
        """
        raise NotImplementedError("update_exceptions is an abstract method")

    def add_dummy_returns(self, name, typ, description):
        """Summary

        Parameters
        ----------
        name : TYPE
            Description
        typ : TYPE
            Description
        description : TYPE
            Description

        Raises
        ------
        NotImplementedError
            Description
        """
        raise NotImplementedError("add_dummy_returns is an abstract method")

    def finalize_section(self, heading, text):
        """Summary

        Parameters
        ----------
        heading : type
            Description
        text : type
            Description
        """
        section = self.SECTION_STYLE(heading, text)
        self.sections[section.alias] = section

    def get_section(self, section_name):
        """Summary

        Parameters
        ----------
        section_name : TYPE
            Description

        Returns
        -------
        TYPE
            Description

        Raises
        ------
        KeyError
            Description
        """
        if section_name in self.sections:
            return self.sections[section_name]
        elif section_name in self.SECTION_STYLE.ALIASES:
            alias = self.SECTION_STYLE.resolve_alias(section_name)
            if alias in self.sections:
                return self.sections[alias]
        raise KeyError("Section '{0}' not found".format(section_name))

    def pop_section(self, section_name):
        """Summary

        Parameters
        ----------
        section_name : TYPE
            Description

        Returns
        -------
        TYPE
            Description

        Raises
        ------
        KeyError
            Description
        """
        if section_name in self.sections:
            return self.sections.pop(section_name)
        elif section_name in self.SECTION_STYLE.ALIASES:
            alias = self.SECTION_STYLE.resolve_alias(section_name)
            if alias in self.sections:
                return self.sections.pop(alias)
        raise KeyError("Section '{0}' not found".format(section_name))

    def insert_section(self, section_name, section):
        """Summary

        Parameters
        ----------
        section_name : TYPE
            Description
        section : TYPE
            Description
        """
        if section.heading != section_name:
            section.heading = section_name
        self.sections[section_name] = section

    def section_exists(self, section_name):
        """Returns ``True`` if section exists, and was finalized.

        Parameters
        ----------
        section_name : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        sec = None
        if section_name in self.sections:
            sec = self.sections[section_name]
        elif section_name in self.SECTION_STYLE.ALIASES:
            alias = self.SECTION_STYLE.resolve_alias(section_name)
            if alias in self.sections:
                sec = self.sections[alias]

        if sec is not None:
            return True
        return False


class NapoleonSection(Section):
    """Summary

    Attributes
    ----------
    ALIASES : TYPE
        Description
    """

    ALIASES = {
        "Args": "Parameters",
        "Arguments": "Parameters",
        "Deleted Args": "Deleted Parameters",
        "Deleted Arguments": "Deleted Parameters",
        "Other Args": "Other Parameters",
        "Other Arguments": "Other Parameters",
        "Keyword Args": "Keyword Arguments",
        "Return": "Returns",
        "Yield": "Yields",
        "No Longer Returns": "No Longer Returned",
        "No Longer Yields": "No Longer Yielded",
        "Warnings": "Warning",
    }

    def is_return_section(self):
        """Summary

        Returns
        -------
        TYPE
            Description
        """
        return self.heading and self.heading.lower() in (
            "return",
            "returns",
            "yield",
            "yields",
        )

    def param_parser_common(self, text):
        """Summary

        Parameters
        ----------
        text : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        # NOTE: there will be some tricky business if there is a
        # section break done by "resuming unindented text"
        param_list = []
        param_dict = OrderedDict()
        text = dedent_docstr(text, 0)

        _r = r"^\S[^\r\n]*(?:\n[^\S\n]+\S[^\r\n]*|\n)*"
        param_blocks = re.findall(_r, text, re.MULTILINE)
        for i, block in enumerate(param_blocks):
            param = self.finalize_param(block, len(param_list))
            param_list.append(param)
            if self.is_return_section():
                param.names = [", ".join(param.names)]
                param_dict[i] = param
            else:
                for name in param.names:
                    param_dict[name] = param
        return param_dict


class NapoleonDocstring(Docstring):  # pylint: disable=abstract-method
    """Styles understood by napoleon, aka. Google/Numpy.

    Attributes
    ----------
    STYLE_NAME : str
        Description
    TEMPLATE : TYPE
        Description
    trailing_newlines : TYPE
        Description
    """

    STYLE_NAME = "napoleon"

    TEMPLATE = OrderedDict(
        [
            ("Summary", None),
            ("Parameters", None),
            ("Keyword Arguments", None),
            ("Returns", None),
            ("Yields", None),
            ("No Longer Returned", None),
            ("No Longer Yielded", None),
            ("Other Parameters", None),
            ("Deleted Parameters", None),
            ("Attributes", None),
            ("Deleted Attributes", None),
            ("Methods", None),
            ("Raises", None),
            ("No Longer Raises", None),
            ("Warns", None),
            ("See Also", None),
            ("Warning", None),
            ("Note", None),
            ("Notes", None),
            ("References", None),
            ("Example", None),
            ("Examples", None),
        ]
    )

    @staticmethod
    def _extract_section_name(sec_re_result):
        """Summary

        Parameters
        ----------
        sec_re_result : TYPE
            Description

        Returns
        -------
        TYPE
            Description
        """
        return sec_re_result.strip()

    def _parse(self, s):
        """Summary

        Parameters
        ----------
        s : type
            Description
        """
        logger.debug("[NapoleonDocstring] starts parsing text")

        self.trailing_newlines = count_trailing_newlines(s)
        s = dedent_docstr(s)

        sec_starts = [
            (m.start(), m.end(), m.string[m.start(): m.end()])
            for m in re.finditer(self.SECTION_RE, s, re.MULTILINE)
        ]

        sec_starts.insert(0, (0, 0, "Summary"))
        sec_starts.append((len(s), len(s), ""))

        for current_sec, next_sec in zip(sec_starts[:-1], sec_starts[1:]):
            sec_name = self._extract_section_name(current_sec[2])
            sec_body = s[current_sec[1]: next_sec[0]]
            self.finalize_section(sec_name, sec_body)

    @staticmethod
    def _format_section_text(heading, body):
        """Summary

        Parameters
        ----------
        heading : TYPE
            Description
        body : TYPE
            Description

        Raises
        ------
        NotImplementedError
            Description
        """
        raise NotImplementedError("This is an abstract method")

    def format(self, top_indent):
        """Summary

        Parameters
        ----------
        top_indent : type
            Description

        Returns
        -------
        TYPE
            Description

        """
        logger.debug("[NapoleonDocstring] starts formatting")

        s = ""
        if self.section_exists("Summary"):
            sec_text = self.get_section("Summary").text
            if sec_text.strip():
                s += with_bounding_newlines(sec_text, nleading=0, ntrailing=1)

        for _, section in islice(self.sections.items(), 1, None):
            if section is None:
                continue

            sec_body = indent_docstr(section.text, section.section_indent, n=0)
            sec_text = self._format_section_text(section.heading, sec_body)
            s += with_bounding_newlines(sec_text, nleading=1, ntrailing=1)

        if self.trailing_newlines:
            s = with_bounding_newlines(s, ntrailing=self.trailing_newlines)
        s = indent_docstr(s, top_indent)

        return s

    def _update_section(
        self,
        params,
        sec_name,
        sec_alias=None,
        del_prefix="Deleted ",
        alpha_order=False,
        other_sections=(),
    ):
        """Update section to add/remove params.

        As a failsafe, params that are removed are placed in a "Deleted ..." section.

        Parameters
        ----------
        params : OrderedDict
            Dictionary of :py:class:`Parameter` objects.
        sec_name : str
            Generic section name.
        sec_alias : str
            Section name that appears in teh docstring.
        del_prefix : str
            Prefix for section that holds params that no longer exist.
        alpha_order : bool
            Whether or not to alphabetically sort the params.
        other_sections : tuple, optional
            Description

        Returns
        -------
        TYPE
            Description
        """
        if not sec_alias:
            sec_alias = sec_name

        if not self.section_exists(sec_name) and len(params) == 0:
            return None
        elif not self.section_exists(sec_name):
            self.finalize_section(sec_alias, "")

        # put together which other sections exist so we can use them to
        # exclude params that exist in them
        _other = []
        for _secname in other_sections:
            if self.section_exists(_secname):
                _other.append(self.get_section(_secname))
        other_sections = _other

        if alpha_order:
            sorted_params = OrderedDict()
            for k in sorted(list(params.keys()), key=str.lower):
                sorted_params[k] = params[k]
            params = sorted_params

        current_dict = self.get_section(sec_name).args

        # go through params in the order of the function declaration
        # and cherry-pick from current_dict if there's already a description
        # for that parameter
        tags_seen = dict()
        new = OrderedDict()
        for name, param in params.items():
            if name in current_dict:
                def_param = param
                param = current_dict.pop(name)

                if param.tag in tags_seen:
                    param = None
                else:
                    tags_seen[param.tag] = True

                # update the type if annotated
                if def_param.annotated:
                    param.types = def_param.types

            else:
                # if param is in one of the 'other sections', then don't
                # worry about it
                for sec in other_sections:
                    if name in sec.args:
                        # update the type if the annotated
                        if param.annotated:
                            sec.args[name].types = param.types
                        # now ignore it
                        param = None
            if param:
                new[name] = param

        # add description only parameters back in
        # NOTE: The list() cast is to avoid the exception caused by modifying current_dict with
        # current_dict.pop(key).
        for key, param in list(current_dict.items()):
            if param.descr_only:
                # param.description = '\n' + param.description
                new[key] = current_dict.pop(key)

        # not sure when this guy gets created
        if "" in current_dict:
            del current_dict[""]

        # go through params that are no linger in the arguments list and
        # move them from the Parameters section of the docstring to the
        # deleted parameters section
        if len(current_dict):
            del_sec_name = del_prefix + sec_name
            del_sec_alias = del_prefix + sec_alias
            logger.warning("Killing parameters named: {}".format(current_dict.keys()))
            # TODO: put a switch here for other bahavior?
            if not self.section_exists(self.SECTION_STYLE.resolve_alias(del_sec_name)):
                self.finalize_section(del_sec_name, "")

            deled_params = self.get_section(del_sec_name)
            deleted_tags = dict()
            for key, val in current_dict.items():
                if key in deled_params.args:
                    logger.warning(
                        "Stronger Warning: Killing old deleted param: " "'{0}'".format(key)
                    )

                val.names.remove(key)
                if val.tag in deleted_tags:
                    deleted_tags[val.tag].names.append(key)
                else:
                    new_val = Parameter([key], val.types, val.description)
                    deleted_tags[val.tag] = new_val
                    deled_params.args[key] = new_val

        if len(new) == 0:
            self.sections[sec_name] = None
        else:
            self.sections[sec_name].args = new

    def update_parameters(self, params):
        """Summary

        Parameters
        ----------
        params : OrderedDict
            Params objects keyed by their names.
        """
        logger.debug("[NapoleonDocstring] update parameters")
        other_sections = ["Other Parameters", "Keyword Parameters"]
        self._update_section(
            params,
            "Parameters",
            self.PREFERRED_PARAMS_ALIAS,
            other_sections=other_sections,
        )

    def update_return_type(
        self,
        ret_name,
        ret_type,
        default_description="Description",
        keyword="return",
        del_prefix="No Longer ",
    ):
        """Summary

        Parameters
        ----------
        ret_name : TYPE
            Description
        ret_type : TYPE
            Description
        default_description : str, optional
            Description
        keyword : str, optional
            Description
        del_prefix : str, optional
            Description

        Returns
        -------
        TYPE
            Description
        """
        logger.debug("[NapoleonDocstring] update return type")

        if keyword == "yield":
            sec_name = "Yields"
        elif keyword == "return":
            sec_name = "Returns"
        else:
            logger.debug("Unknown return keyword: '{}'".format(keyword))

            for std_ret_name in ("Yields", "Returns"):
                if self.section_exists(std_ret_name):
                    del_sec_name = del_prefix + std_ret_name
                    del_sec_alias = self.SECTION_STYLE.resolve_alias(del_sec_name)
                    if not self.section_exists(del_sec_alias):
                        self.finalize_section(del_sec_alias, "")

                    del_sec = self.get_section(del_sec_alias)

                    sec = self.pop_section(std_ret_name)
                    del_sec.args = sec.args

            return

        if not self.section_exists(sec_name):
            # see if a section exists from another keyword, ie, maybe
            # this function used to return, but now it yields
            for std_ret_name in ("Yields", "Returns"):
                if self.section_exists(std_ret_name):
                    # necessary to recreate completly the section
                    # in order to use the right parser and formatter
                    logger.debug("Old return section exists: '{}'".format(std_ret_name))
                    old_sec = self.pop_section(std_ret_name)

                    self.finalize_section(sec_name, "")
                    new_sec = self.get_section(sec_name)
                    new_sec.args = old_sec.args

                    self.insert_section(sec_name, new_sec)
                    break

        if self.section_exists(sec_name):
            sec = self.get_section(sec_name)

            if sec.args and ret_type:
                p0 = next(iter(sec.args.values()))
                if p0.descr_only:
                    p0.description = ret_type
                elif p0.types:
                    p0.types = ret_type
                elif p0.names:
                    p0.names = [ret_type]
            elif ret_name or ret_type:
                description = default_description
                sec.args = OrderedDict()
                if ret_name:
                    sec.args[ret_name] = Parameter([ret_name], ret_type, description)
                else:
                    sec.args[ret_type] = Parameter([ret_type], "", description)
            else:
                # and i ask myself, how did i get here?
                pass
        else:
            self.finalize_section(sec_name, "")
            sec = self.get_section(sec_name)
            ret_type = ret_type if ret_type != "" else "${NUMBER:TYPE}"
            sec.args = OrderedDict()
            sec.args[ret_type] = Parameter([ret_type], "", default_description)

    def update_attributes(
        self, attribs: OrderedDict[str, Parameter], alpha_order: bool = True
    ) -> None:
        """Summary

        Parameters
        ----------
        attribs : OrderedDict
            Params objects keyed by their names.
        alpha_order : bool, optional
            Description
        """
        logger.debug("[NapoleonDocstring] update attributes")
        self._update_section(attribs, "Attributes", alpha_order=alpha_order)

    def update_exceptions(
        self, attribs: OrderedDict[str, Parameter], alpha_order: bool = True
    ) -> None:
        """Summary

        Parameters
        ----------
        attribs : OrderedDict
            Params objects keyed by their names.
        alpha_order : bool, optional
            Description
        """
        logger.debug("[NapoleonDocstring] update exceptions")
        self._update_section(attribs, "Raises", del_prefix="No Longer ", alpha_order=alpha_order)

    def add_dummy_returns(self, name, typ, description):
        """Summary

        Parameters
        ----------
        name : TYPE
            Description
        typ : TYPE
            Description
        description : TYPE
            Description
        """
        # No longer used??
        if not self.section_exists("Returns"):
            sec = self.SECTION_STYLE("Returns")
            if name:
                sec.args = {name: Parameter([name], typ, description)}
            else:
                sec.args = {typ: Parameter([typ], "", description)}
            self.sections["Returns"] = sec
