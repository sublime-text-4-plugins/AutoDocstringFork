# -*- coding: utf-8 -*-
"""Business end of the AutoDocstring plugin

Attributes
----------
all_decl_re : str
    Regular expression to match all declarations.
class_decl_re : str
    Regular expression to match class declarations.
events : Events
    Events manager.
func_decl_re : str
    Regular expression to match function declarations.
logger : logging_system.Logger
    The logger.
plugin_name : str
    Plugin name.
queue : Queue
    Queue manager.
root_folder : str
    The main folder containing the plugin.
settings : settings_utils.SettingsManager
    Settings manager.
"""
from __future__ import annotations

# TODO: custom indentation on parameters
# TODO: check other and kwargs on update_parameters
# TODO: detect first_space used in the current docstring?
# DONE: break this module up into smaller pieces

# TODO: Should I add distinction between private and special attributes?
# TODO: Should I add optional type insertion for annotated function parameters?

import os

import sublime

root_folder: str = os.path.realpath(
    os.path.abspath(
        os.path.join(os.path.normpath(os.path.join(os.path.dirname(__file__), os.pardir)))
    )
)

from python_utils import logging_system
from python_utils import misc_utils
from python_utils.sublime_text_utils import settings as settings_utils
from python_utils.sublime_text_utils.custom_classes import CustomListInputItem
from python_utils.sublime_text_utils.events import Events
from python_utils.sublime_text_utils.queue import Queue

queue: Queue = Queue()
events: Events = Events()
plugin_name: str = "AutoDocstringFork"
logger: logging_system.Logger = logging_system.Logger(
    logger_name=plugin_name,
    use_file_handler=os.path.join(root_folder, "tmp", "logs"),
)
settings: settings_utils.SettingsManager = settings_utils.SettingsManager(
    settings_file=plugin_name,
    events=events,
    logger=logger,
)

from . import docstring_styles

__class_re: str = r"(class)\s+([^\s\(\):]+)\s*(\(([\s\S]*?)\))?"
__func_re: str = r"(?:async\s*)?(def)\s+([^\s\(\):]+)\s*\(([\s\S]*?)\)\s*(->[\s\S]*?)?"

all_decl_re: str = r"^[^\S\n]*({0}|{1})\s*:".format(__class_re, __func_re)
class_decl_re: str = r"^[^\S\n]*{0}\s*:".format(__class_re)
func_decl_re: str = r"^[^\S\n]*{0}\s*:".format(__func_re)


class Storage:
    """Storage class.

    Attributes
    ----------
    all_docstring_styles : list[str]
        List of supported docstring styles.
    all_docstring_styles_quick_items : list[CustomListInputItem]
        List of :any:`CustomListInputItem` items to use in the command palette to represent
        supported docstring styles.
    predefined_summaries : dict[str, str]
        Predefined summaries as defined in the settings file.
    """

    all_docstring_styles: list[str] = []
    all_docstring_styles_quick_items: list[CustomListInputItem] = []
    predefined_summaries: dict[str, str] = {}

    @classmethod
    def update(cls) -> None:
        """Update storage."""
        cls.all_docstring_styles.clear()
        cls.all_docstring_styles_quick_items.clear()
        cls.predefined_summaries.clear()

        cls.all_docstring_styles = [s.lower() for s in docstring_styles.STYLE_LOOKUP.keys()]

        for docstring_style in cls.all_docstring_styles:
            cls.all_docstring_styles_quick_items.append(
                CustomListInputItem(
                    text=docstring_style.title(),
                    value=docstring_style,
                )
            )

        cls.all_docstring_styles_quick_items.sort()

        cls.predefined_summaries = misc_utils.merge_dict(
            settings.get("predefined_summaries", {}),
            settings.get("predefined_summaries_user", {}),
        )


def set_logging_level() -> None:
    """Set logging level."""
    try:
        logger.set_logging_level(logging_level=settings.get("logging_level", "INFO"))
    except Exception as err:
        print(__file__, err)


@events.on("plugin_loaded")
def on_plugin_loaded() -> None:
    """Called on plugin loaded."""
    queue.debounce(
        settings.load,
        delay=100,
        key=f"{plugin_name}-debounce-settings-load",
    )
    queue.debounce(
        set_logging_level,
        delay=200,
        key=f"{plugin_name}-debounce-set-logging-level",
    )
    queue.debounce(Storage.update, delay=1000, key=f"{plugin_name}-debounce-update-storage")


@events.on("plugin_unloaded")
def on_plugin_unloaded() -> None:
    """Called on plugin loaded."""
    settings.unobserve()
    queue.unload()
    events.destroy()


@events.on("settings_changed")
def on_settings_changed(settings_obj) -> None:
    """Called on settings changed.

    Parameters
    ----------
    settings_obj : python_utils.sublime_text_utils.settings.SettingsManager
        An instance of :any:`python_utils.sublime_text_utils.settings.SettingsManager`.
    """
    if settings_obj.has_changed("logging_level"):
        queue.debounce(
            set_logging_level,
            delay=200,
            key=f"{plugin_name}-debounce-set-logging-level",
        )

    if any(
        (
            settings_obj.has_changed("predefined_summaries"),
            settings_obj.has_changed("predefined_summaries_user"),
        )
    ):
        queue.debounce(
            Storage.update,
            delay=500,
            key=f"{plugin_name}-debounce-update-storage",
        )


class SyntaxManager(object):
    """Manages the state of the syntax highlighting.

    This is kind of a hack for return annotations because the default Python grammar falls apart
    when annotations are present.
    """

    _old_syntax: str | None = None

    @classmethod
    def set_syntax(cls, view: sublime.View) -> None:
        """Set MagicPython syntax.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        if settings.get("use_magic_python_syntax_hack", False):
            cls._old_syntax = view.settings().get("syntax")

            if (
                cls._old_syntax.endswith("MagicPython.tmLanguage")
                or "cython" in cls._old_syntax.lower()
            ):
                cls._old_syntax = None
            else:
                logger.debug("Setting view syntax to MagicPython.")
                view.set_syntax_file(
                    "Packages/AutoDocstringFork/.sublime/syntaxes/ADMagicPython.sublime-syntax"
                )

    @classmethod
    def reset_syntax(cls, view: sublime.View) -> None:
        """Restore original view syntax.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        if cls._old_syntax:
            logger.debug("Restoring original view syntax: {}.".format(cls._old_syntax))
            view.set_syntax_file(cls._old_syntax)
        cls._old_syntax = None
