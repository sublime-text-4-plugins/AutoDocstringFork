# -*- coding: utf-8 -*-
"""Python code parsers using :py:mod:`ast` module and also :py:mod:`parso`.

- This module does not make use of Sublime Text's API calls, so it could be used as a standalone \
module. I made it so I can test the parsers in other Python versions.
- :py:mod:`parso` is used to parse function parameters because they are accessible with just one \
function call and each parameter can easily be mapped to their default values. With :py:mod:`ast` \
it's ultra-mega-complicated to handle the parsed parameters and mapping them to their default \
values is practically impossible.
- On the other hand, parsing function exceptions with :py:mod:`ast` is ultra-mega-simple (it returns \
the exception that is raised and its cause if any) compared with how :py:mod:`parso` does it (it \
returns the entire statement unparsed).
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from python_utils.parso.python.tree import Module
    from typing import Any

import ast
import os
import re
import sys

from collections import OrderedDict
from collections import defaultdict
from textwrap import dedent


if __name__ == "__main__":
    root_folder = os.path.realpath(
        os.path.abspath(
            os.path.join(os.path.normpath(os.path.join(os.path.dirname(__file__), os.pardir)))
        )
    )
    sublime_text_config_folder = os.path.normpath(os.path.join(root_folder, *([os.pardir] * 2)))
    sublime_text_lib_folder = os.path.join(sublime_text_config_folder, "Lib", "python38")

    sys.path.insert(0, sublime_text_lib_folder)

    from python_utils import logging_system

    logger: logging_system.Logger = logging_system.Logger(logger_name="ParsersUtils")
else:
    from . import logger

from python_utils import parso
from python_utils.parso.python.tree import Function
from python_utils.parso.python.tree import Param


# NOTE: _func_arg_types and _func_arg_var_types are not used for now. Keep them in case that
# I need to get rid of parso.
_func_arg_types: list[str] = ["posonlyargs", "args", "vararg", "kwonlyargs", "kwarg"]
_func_arg_var_types: list[str] = ["vararg", "kwarg"]
_ignored_func_args: list[str] = ["self", "cls"]
_white_space_re: re.Pattern = re.compile(r"\s", re.MULTILINE)


def cleanup_annotation(dirty_annotation: str | None) -> str | None:
    """Clean up annotation.

    I get annotations as strings, as they are defined, instead of parsing what could be a shear
    amount of nodes. Since annotations could be defined across multiple lines, I need
    to "clean them up" of all white spaces and then add some of them back without line breaks. This
    is to avoid breaking Sphinx builds due to wrong indentation.

    Parameters
    ----------
    dirty_annotation : str | None
        Annotation to clean.

    Returns
    -------
    str | None
        Cleaned annotation.
    """
    if dirty_annotation is None:
        return None

    annotation: str = (((dirty_annotation.strip()).lstrip(":")).rstrip("=")).strip()
    annotation = _white_space_re.sub("", annotation)
    annotation = annotation.replace(",", ", ").replace("|", " | ")
    return annotation


def get_ast_value(node: ast.AST, source: str = None, typ: str = "assign") -> str | None:
    """Get value from an :py:class:`ast.AST` node.

    Parameters
    ----------
    node : ast.AST
        The :py:class:`ast.AST` node to get the value of.
    source : str, optional
        The string that was the source of the parsed nodes.
    typ : str, optional
        A value *type*. Used to decide how to get and/or modify an obtained value.

    Returns
    -------
    str | None
        The value of a node.

    Todo
    ----
    Find a way to protect against recursion errors.
    """
    if node is None:
        return None

    val: Any = None

    if typ == "assign":
        val = ast.get_source_segment(source, node)

        # NOTE: I'm interested only in "first level" assignments.
        if not val or val.count(".") > 1 or val.count("[") > 0:
            val: None
        elif val.startswith("self."):
            val = val.split(".")[1]
    elif typ == "annotation":
        val = cleanup_annotation(ast.get_source_segment(source, node))
    elif typ == "value":
        if isinstance(node, ast.Name):
            val = str(node.id)
        elif isinstance(node, ast.Call):
            val = get_ast_value(node.func, source=source, typ=typ)
        else:
            val = ast.get_source_segment(source, node)

    return val


class BaseVisitor(ast.NodeVisitor):
    """Base node visitor.

    Attributes
    ----------
    parse_type_comments : bool
        Parse type comments.
    source : str
        The string that is the source of the parsed nodes.
    """

    def __init__(self, source: str, parse_type_comments: bool = True) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        source : str
            The string that is the source of the parsed nodes.
        parse_type_comments : bool, optional
            Parse type comments.
        """
        self._data: Any = {}
        self.source: str = source
        self.parse_type_comments: bool = parse_type_comments

    def get_data(self) -> Any:
        """Get collected data.

        Returns
        -------
        Any
            The collected data.
        """
        return self._data

    def destroy_data(self) -> None:
        """Destroy data."""
        self._data = None

    def _get_assignment(self, node: ast.AST) -> str | None:
        """Get the value of a node that represents an assignment.

        Parameters
        ----------
        node : ast.AST
            The :py:class:`ast.AST` node to get the value from.

        Returns
        -------
        str | None
            The value of the node.
        """
        return get_ast_value(node, source=self.source, typ="assign")

    def _get_value(self, node: ast.AST) -> str | None:
        """Get the value of a node that represents a value.

        Parameters
        ----------
        node : ast.AST
            The :py:class:`ast.AST` node to get the value from.

        Returns
        -------
        str | None
            The value of the node.
        """
        return get_ast_value(node, source=self.source, typ="value")

    def _get_annotation(self, node: ast.AST) -> str | None:
        """Get the value of a node that represents an annotation.

        Parameters
        ----------
        node : ast.AST
            The :py:class:`ast.AST` node to get the value from.

        Returns
        -------
        str | None
            The value of the node.
        """
        return get_ast_value(node, source=self.source, typ="annotation")

    def _check_condition(self, node: ast.AST) -> bool:
        """Check if the passed node is a desired node.

        Parameters
        ----------
        node : ast.AST
            The`:py:class:`ast.AST` node to check.

        Raises
        ------
        NotImplementedError
            Method should be declared in a sub-class.
        """
        raise NotImplementedError

    def _store_attributes(self, node: ast.AST) -> None:
        """Store class or module attributes.

        Parameters
        ----------
        node : ast.AST
            The :py:class:`ast.AST` node that represents a statement.
        """
        try:
            assign: str = ""
            value: str = ""
            anno: str = ""

            if isinstance(node, ast.Assign):
                if node.targets and self._check_condition(node.targets[0]):
                    assign = self._get_assignment(node.targets[0])
                    value = self._get_value(node.value)
                    anno = node.type_comment if self.parse_type_comments else None

                    if assign and assign not in self._data["attributes"]:
                        self._data["attributes"][assign] = (value, anno)
            elif isinstance(node, ast.AnnAssign):
                if self._check_condition(node.target):
                    assign = self._get_assignment(node.target)
                    value = self._get_value(node.value)
                    anno = self._get_annotation(node.annotation)

                    if assign and assign not in self._data["attributes"]:
                        self._data["attributes"][assign] = (value, anno)
        except Exception as err:
            logger.exception(err)


class ClassDefVisitor(BaseVisitor):
    """Class definition visitor.

    This visitor collects all attributes defined in a class body and all instance attributes
    defined by every class method.
    """

    def __init__(self, *args, **kwargs) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        *args
            Arguments.
        **kwargs
            Keyword arguments.
        """
        super().__init__(*args, **kwargs)
        self._data: dict[str, str | OrderedDict[str, tuple[str, Any]]] = {
            "name": "",
            "attributes": OrderedDict(),
        }
        self._handling_func_def: bool = False
        self._callables: list[ast.FunctionDef | ast.AsyncFunctionDef] = []

    def visit_ClassDef(self, class_def: ast.ClassDef) -> None:
        """Visit class definition.

        Parameters
        ----------
        class_def : ast.ClassDef
            The :py:class:`ast.AST` node to visit.
        """
        self._data["name"] = class_def.name

        for node in class_def.body:
            if isinstance(node, (ast.AsyncFunctionDef, ast.FunctionDef)):
                self._callables.append(node)
            else:
                self._store_attributes(node)

        if self._callables:
            self._handling_func_def = True

            for _callable in self._callables:
                self._visit_callable(_callable)

    def _visit_callable(self, _callable: ast.FunctionDef | ast.AsyncFunctionDef) -> None:
        """Visit function definition.

        Parameters
        ----------
        _callable : ast.FunctionDef | ast.AsyncFunctionDef
            The function definition to visit.
        """
        for node in _callable.body:
            self._store_attributes(node)

    def _check_condition(self, node: ast.AST) -> bool:
        """See :py:meth:`BaseVisitor._check_condition`.

        Parameters
        ----------
        node : ast.AST
            See :py:meth:`BaseVisitor._check_condition`.

        Returns
        -------
        bool
            See :py:meth:`BaseVisitor._check_condition`.
        """
        if self._handling_func_def:
            return (
                isinstance(node, ast.Attribute)
                and isinstance(node.value, ast.Name)
                and node.value.id == "self"
            )
        else:
            return isinstance(node, ast.Name)


class ModuleVisitor(BaseVisitor):
    """Module visitor.

    This visitor collects all attributes defined in a module.
    """

    def __init__(self, *args, **kwargs) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        *args
            Arguments.
        **kwargs
            Keyword arguments.
        """
        super().__init__(*args, **kwargs)
        self._data: dict[str, OrderedDict[str, tuple[str, Any]]] = {
            "attributes": OrderedDict(),
        }

    def visit_Module(self, module_def: Any) -> None:
        """Visit module.

        Parameters
        ----------
        module_def : Any
            The :py:class:`ast.AST` node to visit.
        """
        for node in module_def.body:
            self._store_attributes(node)

    def _check_condition(self, node: Any) -> bool:
        """See :py:meth:`BaseVisitor._check_condition`.

        Parameters
        ----------
        node : Any
            See :py:meth:`BaseVisitor._check_condition`.

        Returns
        -------
        bool
            See :py:meth:`BaseVisitor._check_condition`.
        """
        return isinstance(node, ast.Name)


class FunctionDefVisitor(BaseVisitor):
    """Function definition visitor.

    This visitor collects all function parameters, the exceptions that it raises and the return
    annotation if present.
    """

    def __init__(self, *args, **kwargs) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        *args
            Arguments.
        **kwargs
            Keyword arguments.
        """
        super().__init__(*args, **kwargs)
        self._data: defaultdict = defaultdict(list)
        self._args_type_comments: dict = {}
        self._function_type_comments_args: list[str] = []
        self._function_type_comments_returns: str | None = None

    def _visit_callable(self, _callable: ast.FunctionDef | ast.AsyncFunctionDef) -> None:
        """Visit callable.

        Parameters
        ----------
        _callable : ast.FunctionDef | ast.AsyncFunctionDef
            The :py:class:`ast.AST` node to visit.
        """
        self._data["name"].append(_callable.name)

        if self.parse_type_comments:
            self._store_type_comments(_callable)

        self._store_parso_args(_callable)
        self._store_returns(_callable)
        self._store_exceptions(_callable)
        self._store_return_keyword(_callable)

    def visit_AsyncFunctionDef(self, async_func_def: ast.AsyncFunctionDef) -> None:
        """Visit ``async`` function.

        Parameters
        ----------
        async_func_def : ast.AsyncFunctionDef
            The :py:class:`ast.AST` node to visit.
        """
        self._visit_callable(async_func_def)

    def visit_FunctionDef(self, func_def: ast.FunctionDef) -> None:
        """Visit function.

        Parameters
        ----------
        func_def : ast.FunctionDef
            The :py:class:`ast.AST` node to visit.
        """
        self._visit_callable(func_def)

    def _store_return_keyword(self, _callable: ast.FunctionDef | ast.AsyncFunctionDef) -> None:
        """Store return keyword.

        Parameters
        ----------
        _callable : ast.FunctionDef | ast.AsyncFunctionDef
            The callable from which to extract return keywords.
        """
        visitor: ReturnKeywordsVisitor = ReturnKeywordsVisitor(
            ast.get_source_segment(self.source, _callable)
        )
        visitor.visit(_callable)
        self._data["return_keyword"].append(visitor.get_data())

    def _store_exceptions(self, _callable: ast.FunctionDef | ast.AsyncFunctionDef) -> None:
        """Store exceptions.

        Parameters
        ----------
        _callable : ast.FunctionDef | ast.AsyncFunctionDef
            The callable from which to extract exceptions.
        """
        exc_visitor: ExceptionsVisitor = ExceptionsVisitor(
            ast.get_source_segment(self.source, _callable)
        )
        exc_visitor.visit(_callable)
        self._data["exceptions"].extend(list(exc_visitor.get_data()))

    def _store_returns(self, _callable: ast.FunctionDef | ast.AsyncFunctionDef) -> None:
        """Store returns.

        Parameters
        ----------
        _callable : ast.FunctionDef | ast.AsyncFunctionDef
            The callable from which to extract the return annotation.
        """
        ret_anno = self._get_annotation(_callable.returns)

        if ret_anno is None and self.parse_type_comments:
            ret_anno = self._function_type_comments_returns

        self._data["returns"].append(ret_anno)

    def _store_type_comments(self, _callable: ast.FunctionDef | ast.AsyncFunctionDef) -> None:
        """Store type comments.

        Parameters
        ----------
        _callable : ast.FunctionDef | ast.AsyncFunctionDef
            The callable from which to extract type comments.
        """
        function_type_comment: str = _callable.type_comment
        arg_trees: list[ast.arg] = []

        # BEWERE: Pay attention to the ast.get_source_segment() calls. The source is the type comment
        # itself, not self.source.
        if function_type_comment:
            type_comment_tree = ast.parse(function_type_comment, mode="func_type")

            for t in type_comment_tree.argtypes:
                self._function_type_comments_args.append(
                    cleanup_annotation(ast.get_source_segment(function_type_comment, t))
                )

            self._function_type_comments_returns = cleanup_annotation(
                ast.get_source_segment(function_type_comment, type_comment_tree.returns)
            )

        # NOTE: Smash together all ast.arg found. I'm not interested in what type of arguments they
        # are or their order, just in their names to be mapped to their type comments.
        arg_trees.extend(_callable.args.posonlyargs)
        arg_trees.extend(_callable.args.args)
        arg_trees.extend(_callable.args.kwonlyargs)

        if _callable.args.vararg:
            arg_trees.append(_callable.args.vararg)

        if _callable.args.kwarg:
            arg_trees.append(_callable.args.kwarg)

        if arg_trees:
            for arg in arg_trees:
                if arg.type_comment:
                    self._args_type_comments[arg.arg] = arg.type_comment

    def _store_parso_args(self, _callable: ast.FunctionDef | ast.AsyncFunctionDef) -> None:
        """Store ``parso`` arguments.

        Parameters
        ----------
        _callable : ast.FunctionDef | ast.AsyncFunctionDef
            The callable from which to extract arguments.

        Note
        ----
        I'm forced to use ``parso`` because it's impossible to map parameter names to their values
        with :py:class:`ast.arguments`. It makes no sense what-so-f*cking-ever.
        """
        try:
            # WARNING: Do not use internal functions to handle nodes. All nodes inside this function
            # are :py:mod:`parso` nodes; all internal functions expect :py:class:`ast.AST` nodes.
            callable_tree: Module = parso.parse(
                dedent(ast.get_source_segment(self.source, _callable))
            )
            callable_node: Function | None = None

            # NOTE: I have to iterate function definitions because 'parso' *wraps* 'async' function
            # definitions inside another 'PythonBaseNode' class with two children
            # (Keyword and Function).
            for c in callable_tree.iter_funcdefs():
                if isinstance(c, Function):
                    callable_node = c
                    break

            if callable_node is not None:
                func_params: list[Param] = callable_node.get_params()
                first_arg_offset: int = 0

                for param in func_params:
                    if not isinstance(param, Param) or not param.name:
                        continue

                    star: str = param.star_count * "*"
                    # NOTE: I can't remember why I opted for the white space eradication regular
                    # expression instead of just strip()ing.
                    name: str = _white_space_re.sub("", param.name.get_code(include_prefix=False))

                    if param.position_index == 0 and name in _ignored_func_args:
                        first_arg_offset += 1
                        continue

                    pos: int = param.position_index - first_arg_offset
                    value: str | None = None

                    if param.default is not None:
                        value = _white_space_re.sub(
                            "", param.default.get_code(include_prefix=False)
                        )

                    anno: str | None = (
                        None
                        if param.annotation is None
                        else cleanup_annotation(param.annotation.get_code(include_prefix=False))
                    )

                    # NOTE: I treat type comments as annotations. But I want to implement optional
                    # insertion of types in docstrings when function parameters are annotated, as
                    # per Numpy specifications. Right now I can't figure out how to do it, but I
                    # will store this boolean for the future.
                    is_annotated: bool = anno is not None

                    if anno is None and self.parse_type_comments:
                        # NOTE: The default fallback of get() is a trick to safely get an element
                        # from a list.
                        # <3 https://stackoverflow.com/a/41387299
                        anno = self._args_type_comments.get(
                            name,
                            (self._function_type_comments_args[pos: pos + 1] or (None,))[0],
                        )

                    name = star + name

                    self._data["args"].append((name, value, anno, is_annotated))
        except Exception as err:
            logger.exception(err)


class ExceptionsVisitor(BaseVisitor):
    """Exceptions visitor.

    This visitor collects all raised exceptions.

    Note
    ----
    Right now I only traverse the ``raise`` statements. Should I also traverse the
    ``try/except`` blocks?
    """

    def __init__(self, *args, **kwargs) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        *args
            Arguments.
        **kwargs
            Keyword arguments.
        """
        super().__init__(*args, **kwargs)
        self._data: set[str] = set()

    def visit_Raise(self, raise_def: ast.Raise) -> None:
        """Visit ``raise`` definition.

        Parameters
        ----------
        raise_def : ast.Raise
            The :py:class:`ast.AST` node to visit.
        """
        try:
            exc = self._get_value(raise_def.exc)

            if exc is not None:
                self._data.add(exc)
        except Exception as err:
            logger.exception(err)


class ReturnKeywordsVisitor(BaseVisitor):
    """Return keywords visitor.

    This visitor collects all return keywords (``return`` and ``yield``) and returns the last one found.
    """

    def __init__(self, *args, **kwargs) -> None:
        """See :py:meth:`object.__init__`.

        Parameters
        ----------
        *args
            Arguments.
        **kwargs
            Keyword arguments.
        """
        super().__init__(*args, **kwargs)
        self._data: list[tuple[int, str]] = []

    def get_data(self) -> str:
        """Get collected data.

        Returns
        -------
        str
            The return keyword.
        """
        return_keywords: list[tuple[int, str]] = sorted(self._data)

        try:
            return return_keywords[-1][1]
        except IndexError:
            return ""

    def visit_Return(self, return_def: ast.Return) -> None:
        """Visit ``return`` definition.

        Parameters
        ----------
        return_def : ast.Return
            The :py:class:`ast.AST` node to visit.
        """
        self._data.append((return_def.lineno, "return"))

    def visit_YieldFrom(self, yield_from_def: ast.YieldFrom) -> None:
        """Visit ``yield from`` definition.

        Parameters
        ----------
        yield_from_def : ast.YieldFrom
            The :py:class:`ast.AST` node to visit.
        """
        self._data.append((yield_from_def.lineno, "yield"))

    def visit_Yield(self, yield_def: ast.Yield) -> None:
        """Visit ``yield`` definition.

        Parameters
        ----------
        yield_def : ast.Yield
            The :py:class:`ast.AST` node to visit.
        """
        self._data.append((yield_def.lineno, "yield"))
