# -*- coding: utf-8 -*-
"""Autodoc function.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from .docstring_styles.napoleon import NapoleonDocstring
    from typing import Any

import re

from collections import OrderedDict

import sublime

from . import Storage
from . import class_decl_re
from . import docstring_styles
from . import func_decl_re
from . import logger
from . import settings
from .parsers import parse_class
from .parsers import parse_function
from .parsers import parse_module_attributes
from .utils import find_preceding_declaration
from .utils import get_docstring
from .utils import get_indentation
from .utils import snipify


def autodoc(
    view: sublime.View,
    edit: sublime.Edit,
    region: sublime.Region,
    all_defs: list[sublime.Region],
    desired_style: type[NapoleonDocstring],
    file_type: Any = NotImplemented,
    default_qstyle: str | None = None,
    update_only: bool = False,
) -> None:
    """Actually do the business of auto-documenting.

    Parameters
    ----------
    view : sublime.View
        The current Sublime Text ``View`` object.
    edit : sublime.Edit
        The current Sublime Text ``Edit`` object.
    region : sublime.Region
        Region to look backward from to find a definition, usually gotten with ``view.sel()``.
    all_defs : list[sublime.Region]
        List of declaration regions representing all valid declarations.
    desired_style : type[NapoleonDocstring]
        Subclass of ``Docstring``.
    file_type : Any, optional
        One of ``python`` or ``cython``, not yet implemented.
    default_qstyle : str | None, optional
        Default docstring quotes style.
    update_only : bool, optional
        Update a docstring without editing a buffer.

    Returns
    -------
    None
        Halt execution.

    Raises
    ------
    RuntimeError
        No declaration type could be ascertained.
    """
    default_description: str = settings.get("default_description", "Description")
    default_type: str = settings.get("default_type", "TYPE")
    use_snippet: bool = settings.get("use_snippet", False)
    start_with_newline: bool | str | list[str] = settings.get("start_with_newline", False)

    attribs: OrderedDict[str, docstring_styles.Parameter] | None = None
    func_params: OrderedDict[str, docstring_styles.Parameter] | None = None
    func_exceptions: OrderedDict[str, docstring_styles.Parameter] | None = None
    func_ret_ano: str = ""
    func_ret_keyword: str = ""
    decl_type: str = ""
    target_name: str = ""
    old_ds_whole_region: sublime.Region | None = None
    old_ds_region: sublime.Region | None = None
    quote_style: str = ""
    is_new: bool = False
    is_module_level: bool = False

    if not default_qstyle or settings.get("force_default_qstyle", True):
        default_qstyle = settings.get("default_qstyle", '"""')

    target: sublime.Region = find_preceding_declaration(view, all_defs, region)

    logger.debug("TARGET:: {}".format(target))

    _module_flag: bool = target.a == target.b == 0
    logger.debug("-> found target {} {}".format(target, _module_flag))

    (old_ds_whole_region, old_ds_region, quote_style, is_new, is_module_level) = get_docstring(
        view,
        None if update_only else edit,
        target,
        default_qstyle=default_qstyle,
        extra_class_newlines=settings.get("extra_class_newlines", False),
    )
    if update_only and old_ds_whole_region is None:
        return

    old_docstr: str = view.substr(old_ds_region)

    ds: NapoleonDocstring = docstring_styles.make_docstring_obj(
        old_docstr, desired_style, template_order=settings.get("template_order", True)
    )

    # if start_with_newline was given as a comma separated list of styles,
    # then turn that into a bool of whether or not ds.STYLE_NAME is in the
    # list
    if isinstance(start_with_newline, str):
        start_with_newline = start_with_newline.split(",")
        start_with_newline = [s.strip().lower() for s in start_with_newline]
        start_with_newline = ds.STYLE_NAME in start_with_newline

    # get declaration info
    if _module_flag:
        if settings.get("inspect_module_attributes", True):
            attribs = parse_module_attributes(view, default_type, default_description)
            ds.update_attributes(attribs, alpha_order=settings.get("sort_module_attributes", True))
    else:
        decl_str = view.substr(target).lstrip()

        if decl_str.startswith(("def", "async")):
            decl_type = re.match(func_decl_re, decl_str, re.MULTILINE).group(1)
        elif decl_str.startswith("class"):
            decl_type = re.match(class_decl_re, decl_str, re.MULTILINE).group(1)
        else:
            raise RuntimeError("No declaration type could be ascertained.")

        if decl_type == "def":
            (
                target_name,
                func_params,
                func_ret_ano,
                func_ret_keyword,
                func_exceptions,
            ) = parse_function(
                view,
                target,
                default_type,
                default_description,
                optional_tag=settings.get("optional_tag", "optional"),
            )

            if settings.get("inspect_function_parameters", True):
                ret_name: str = ""
                ret_type: str = func_ret_ano if func_ret_ano else ""

                # prepare return name/type for new / updated returns section
                if is_new and target_name != "__init__":
                    ret_name = settings.get("default_return_name", "")
                    ret_type = func_ret_ano if func_ret_ano else default_type

                ret_name = snipify(ret_name, use_snippet)
                ret_type = snipify(ret_type, use_snippet)

                ds.update_parameters(func_params)
                ds.update_return_type(
                    ret_name,
                    ret_type,
                    default_description=snipify(default_description, use_snippet),
                    keyword=func_ret_keyword,
                )

            if settings.get("inspect_exceptions", True):
                ds.update_exceptions(
                    func_exceptions, alpha_order=settings.get("sort_exceptions", True)
                )
        elif decl_type == "class":
            if settings.get("inspect_class_attributes", True):
                target_name, attribs = parse_class(view, target, default_type, default_description)

                ds.update_attributes(
                    attribs, alpha_order=settings.get("sort_class_attributes", True)
                )

    if is_new:
        snippet_summary: str = ""
        if start_with_newline:
            snippet_summary += "\n"

        if (
            settings.get("use_predefined_summaries", True)
            and target_name in Storage.predefined_summaries
        ):
            snippet_summary += snipify(Storage.predefined_summaries[target_name])
        else:
            snippet_summary += snipify(settings.get("default_summary", "Summary"))

        ds.finalize_section("Summary", snippet_summary)

    # -> create new docstring from meta
    new_ds: NapoleonDocstring = desired_style(ds)

    # -> replace old docstring with the new docstring
    body_indent_txt: str = ""

    if not use_snippet or not is_module_level:
        _, body_indent_txt, _ = get_indentation(view, target, _module_flag)

    new_docstr: str = new_ds.format(body_indent_txt) + body_indent_txt

    # replace ${NUMBER:.*} with ${[0-9]+:.*}
    i: int = 1
    _nstr: str = r"${NUMBER:"
    while new_docstr.find(_nstr) > -1:
        if use_snippet:
            # for snippets
            new_docstr = new_docstr.replace(_nstr, f"${{{i}:", 1)
        else:
            # remove snippet markers
            loc = new_docstr.find(_nstr)
            new_docstr = new_docstr.replace(_nstr, "", 1)
            b_loc = new_docstr.find(r"}", loc)
            new_docstr = new_docstr[:b_loc] + new_docstr[b_loc + 1:]
        i += 1

    if settings.get("keep_previous", False):
        new_docstr = (
            "{0}\n"
            "******* PREVIOUS DOCSTRING *******\n"
            "{1}\n"
            "^^^^^^^ PREVIOUS DOCSTRING ^^^^^^^\n"
            "".format(new_docstr, old_docstr)
        )

    # actually insert the new docstring
    if use_snippet:
        view.replace(edit, old_ds_whole_region, "")
        view.sel().clear()
        view.sel().add(sublime.Region(old_ds_whole_region.a))
        new_docstr = quote_style + new_docstr + quote_style
        view.run_command("insert_snippet", {"contents": new_docstr})
    else:
        view.replace(edit, old_ds_region, new_docstr)
